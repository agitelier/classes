import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class GroupUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('coursgroupesApp.group.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  numberInput: ElementFinder = element(by.css('input#group-number'));

  teacherIDInput: ElementFinder = element(by.css('input#group-teacherID'));

  descriptionInput: ElementFinder = element(by.css('textarea#group-description'));

  studentSelect = element(by.css('select#group-student'));

  coursSelect = element(by.css('select#group-cours'));
}
