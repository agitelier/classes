import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class StudentUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('coursgroupesApp.student.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  emailInput: ElementFinder = element(by.css('input#student-email'));
}
