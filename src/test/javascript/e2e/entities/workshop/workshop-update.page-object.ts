import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class WorkshopUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('coursgroupesApp.workshop.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#workshop-name'));

  urlInput: ElementFinder = element(by.css('input#workshop-url'));

  descriptionInput: ElementFinder = element(by.css('textarea#workshop-description'));
}
