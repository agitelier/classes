import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class VariantUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('coursgroupesApp.variant.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  nameInput: ElementFinder = element(by.css('input#variant-name'));

  descriptionInput: ElementFinder = element(by.css('textarea#variant-description'));

  workshopSelect = element(by.css('select#variant-workshop'));
}
