import { by, element, ElementFinder } from 'protractor';

import AlertPage from '../../page-objects/alert-page';

export default class CoursUpdatePage extends AlertPage {
  title: ElementFinder = element(by.id('coursgroupesApp.cours.home.createOrEditLabel'));
  footer: ElementFinder = element(by.id('footer'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));

  symbolInput: ElementFinder = element(by.css('input#cours-symbol'));

  nameInput: ElementFinder = element(by.css('input#cours-name'));

  descriptionInput: ElementFinder = element(by.css('textarea#cours-description'));

  variantSelect = element(by.css('select#cours-variant'));
}
