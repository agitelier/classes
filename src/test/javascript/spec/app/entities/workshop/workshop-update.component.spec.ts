/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import WorkshopUpdateComponent from '@/entities/workshop/workshop-update.vue';
import WorkshopClass from '@/entities/workshop/workshop-update.component';
import WorkshopService from '@/entities/workshop/workshop.service';

import VariantService from '@/entities/variant/variant.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Workshop Management Update Component', () => {
    let wrapper: Wrapper<WorkshopClass>;
    let comp: WorkshopClass;
    let workshopServiceStub: SinonStubbedInstance<WorkshopService>;

    beforeEach(() => {
      workshopServiceStub = sinon.createStubInstance<WorkshopService>(WorkshopService);

      wrapper = shallowMount<WorkshopClass>(WorkshopUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          workshopService: () => workshopServiceStub,

          variantService: () => new VariantService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: '123' };
        comp.workshop = entity;
        workshopServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(workshopServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.workshop = entity;
        workshopServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(workshopServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
