/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import WorkshopDetailComponent from '@/entities/workshop/workshop-details.vue';
import WorkshopClass from '@/entities/workshop/workshop-details.component';
import WorkshopService from '@/entities/workshop/workshop.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Workshop Management Detail Component', () => {
    let wrapper: Wrapper<WorkshopClass>;
    let comp: WorkshopClass;
    let workshopServiceStub: SinonStubbedInstance<WorkshopService>;

    beforeEach(() => {
      workshopServiceStub = sinon.createStubInstance<WorkshopService>(WorkshopService);

      wrapper = shallowMount<WorkshopClass>(WorkshopDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { workshopService: () => workshopServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundWorkshop = { id: '123' };
        workshopServiceStub.find.resolves(foundWorkshop);

        // WHEN
        comp.retrieveWorkshop('123');
        await comp.$nextTick();

        // THEN
        expect(comp.workshop).toBe(foundWorkshop);
      });
    });
  });
});
