/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import WorkshopComponent from '@/entities/workshop/workshop.vue';
import WorkshopClass from '@/entities/workshop/workshop.component';
import WorkshopService from '@/entities/workshop/workshop.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Workshop Management Component', () => {
    let wrapper: Wrapper<WorkshopClass>;
    let comp: WorkshopClass;
    let workshopServiceStub: SinonStubbedInstance<WorkshopService>;

    beforeEach(() => {
      workshopServiceStub = sinon.createStubInstance<WorkshopService>(WorkshopService);
      workshopServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<WorkshopClass>(WorkshopComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          workshopService: () => workshopServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      workshopServiceStub.retrieve.resolves({ headers: {}, data: [{ id: '123' }] });

      // WHEN
      comp.retrieveAllWorkshops();
      await comp.$nextTick();

      // THEN
      expect(workshopServiceStub.retrieve.called).toBeTruthy();
      expect(comp.workshops[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      workshopServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: '123' });
      comp.removeWorkshop();
      await comp.$nextTick();

      // THEN
      expect(workshopServiceStub.delete.called).toBeTruthy();
      expect(workshopServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
