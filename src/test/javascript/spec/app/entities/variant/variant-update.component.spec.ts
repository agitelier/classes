/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import VariantUpdateComponent from '@/entities/variant/variant-update.vue';
import VariantClass from '@/entities/variant/variant-update.component';
import VariantService from '@/entities/variant/variant.service';

import WorkshopService from '@/entities/workshop/workshop.service';

import CoursService from '@/entities/cours/cours.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Variant Management Update Component', () => {
    let wrapper: Wrapper<VariantClass>;
    let comp: VariantClass;
    let variantServiceStub: SinonStubbedInstance<VariantService>;

    beforeEach(() => {
      variantServiceStub = sinon.createStubInstance<VariantService>(VariantService);

      wrapper = shallowMount<VariantClass>(VariantUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          variantService: () => variantServiceStub,

          workshopService: () => new WorkshopService(),

          coursService: () => new CoursService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: '123' };
        comp.variant = entity;
        variantServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(variantServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.variant = entity;
        variantServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(variantServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
