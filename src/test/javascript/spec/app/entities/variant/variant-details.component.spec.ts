/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import VariantDetailComponent from '@/entities/variant/variant-details.vue';
import VariantClass from '@/entities/variant/variant-details.component';
import VariantService from '@/entities/variant/variant.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Variant Management Detail Component', () => {
    let wrapper: Wrapper<VariantClass>;
    let comp: VariantClass;
    let variantServiceStub: SinonStubbedInstance<VariantService>;

    beforeEach(() => {
      variantServiceStub = sinon.createStubInstance<VariantService>(VariantService);

      wrapper = shallowMount<VariantClass>(VariantDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { variantService: () => variantServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundVariant = { id: '123' };
        variantServiceStub.find.resolves(foundVariant);

        // WHEN
        comp.retrieveVariant('123');
        await comp.$nextTick();

        // THEN
        expect(comp.variant).toBe(foundVariant);
      });
    });
  });
});
