/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import SubGroupDetailComponent from '@/entities/sub-group/sub-group-details.vue';
import SubGroupClass from '@/entities/sub-group/sub-group-details.component';
import SubGroupService from '@/entities/sub-group/sub-group.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('SubGroup Management Detail Component', () => {
    let wrapper: Wrapper<SubGroupClass>;
    let comp: SubGroupClass;
    let subGroupServiceStub: SinonStubbedInstance<SubGroupService>;

    beforeEach(() => {
      subGroupServiceStub = sinon.createStubInstance<SubGroupService>(SubGroupService);

      wrapper = shallowMount<SubGroupClass>(SubGroupDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { subGroupService: () => subGroupServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundSubGroup = { id: '123' };
        subGroupServiceStub.find.resolves(foundSubGroup);

        // WHEN
        comp.retrieveSubGroup('123');
        await comp.$nextTick();

        // THEN
        expect(comp.subGroup).toBe(foundSubGroup);
      });
    });
  });
});
