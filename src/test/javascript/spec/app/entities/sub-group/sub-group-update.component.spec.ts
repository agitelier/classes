/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import SubGroupUpdateComponent from '@/entities/sub-group/sub-group-update.vue';
import SubGroupClass from '@/entities/sub-group/sub-group-update.component';
import SubGroupService from '@/entities/sub-group/sub-group.service';

import StudentService from '@/entities/student/student.service';

import GroupService from '@/entities/group/group.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('SubGroup Management Update Component', () => {
    let wrapper: Wrapper<SubGroupClass>;
    let comp: SubGroupClass;
    let subGroupServiceStub: SinonStubbedInstance<SubGroupService>;

    beforeEach(() => {
      subGroupServiceStub = sinon.createStubInstance<SubGroupService>(SubGroupService);

      wrapper = shallowMount<SubGroupClass>(SubGroupUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          subGroupService: () => subGroupServiceStub,

          studentService: () => new StudentService(),

          groupService: () => new GroupService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: '123' };
        comp.subGroup = entity;
        subGroupServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(subGroupServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.subGroup = entity;
        subGroupServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(subGroupServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
