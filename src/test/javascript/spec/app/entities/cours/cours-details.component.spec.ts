/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import * as config from '@/shared/config/config';
import CoursDetailComponent from '@/entities/cours/cours-details.vue';
import CoursClass from '@/entities/cours/cours-details.component';
import CoursService from '@/entities/cours/cours.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('router-link', {});

describe('Component Tests', () => {
  describe('Cours Management Detail Component', () => {
    let wrapper: Wrapper<CoursClass>;
    let comp: CoursClass;
    let coursServiceStub: SinonStubbedInstance<CoursService>;

    beforeEach(() => {
      coursServiceStub = sinon.createStubInstance<CoursService>(CoursService);

      wrapper = shallowMount<CoursClass>(CoursDetailComponent, {
        store,
        i18n,
        localVue,
        provide: { coursService: () => coursServiceStub },
      });
      comp = wrapper.vm;
    });

    describe('OnInit', () => {
      it('Should call load all on init', async () => {
        // GIVEN
        const foundCours = { id: '123' };
        coursServiceStub.find.resolves(foundCours);

        // WHEN
        comp.retrieveCours('123');
        await comp.$nextTick();

        // THEN
        expect(comp.cours).toBe(foundCours);
      });
    });
  });
});
