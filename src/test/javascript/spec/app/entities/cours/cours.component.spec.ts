/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import CoursComponent from '@/entities/cours/cours.vue';
import CoursClass from '@/entities/cours/cours.component';
import CoursService from '@/entities/cours/cours.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
localVue.component('font-awesome-icon', {});
localVue.component('b-alert', {});
localVue.component('b-badge', {});
localVue.directive('b-modal', {});
localVue.component('b-button', {});
localVue.component('router-link', {});

const bModalStub = {
  render: () => {},
  methods: {
    hide: () => {},
    show: () => {},
  },
};

describe('Component Tests', () => {
  describe('Cours Management Component', () => {
    let wrapper: Wrapper<CoursClass>;
    let comp: CoursClass;
    let coursServiceStub: SinonStubbedInstance<CoursService>;

    beforeEach(() => {
      coursServiceStub = sinon.createStubInstance<CoursService>(CoursService);
      coursServiceStub.retrieve.resolves({ headers: {} });

      wrapper = shallowMount<CoursClass>(CoursComponent, {
        store,
        i18n,
        localVue,
        stubs: { bModal: bModalStub as any },
        provide: {
          alertService: () => new AlertService(store),
          coursService: () => coursServiceStub,
        },
      });
      comp = wrapper.vm;
    });

    it('Should call load all on init', async () => {
      // GIVEN
      coursServiceStub.retrieve.resolves({ headers: {}, data: [{ id: '123' }] });

      // WHEN
      comp.retrieveAllCourss();
      await comp.$nextTick();

      // THEN
      expect(coursServiceStub.retrieve.called).toBeTruthy();
      expect(comp.cours[0]).toEqual(jasmine.objectContaining({ id: '123' }));
    });
    it('Should call delete service on confirmDelete', async () => {
      // GIVEN
      coursServiceStub.delete.resolves({});

      // WHEN
      comp.prepareRemove({ id: '123' });
      comp.removeCours();
      await comp.$nextTick();

      // THEN
      expect(coursServiceStub.delete.called).toBeTruthy();
      expect(coursServiceStub.retrieve.callCount).toEqual(2);
    });
  });
});
