/* tslint:disable max-line-length */
import { shallowMount, createLocalVue, Wrapper } from '@vue/test-utils';
import sinon, { SinonStubbedInstance } from 'sinon';
import Router from 'vue-router';

import AlertService from '@/shared/alert/alert.service';
import * as config from '@/shared/config/config';
import CoursUpdateComponent from '@/entities/cours/cours-update.vue';
import CoursClass from '@/entities/cours/cours-update.component';
import CoursService from '@/entities/cours/cours.service';

import GroupService from '@/entities/group/group.service';

import VariantService from '@/entities/variant/variant.service';

const localVue = createLocalVue();

config.initVueApp(localVue);
const i18n = config.initI18N(localVue);
const store = config.initVueXStore(localVue);
const router = new Router();
localVue.use(Router);
localVue.component('font-awesome-icon', {});

describe('Component Tests', () => {
  describe('Cours Management Update Component', () => {
    let wrapper: Wrapper<CoursClass>;
    let comp: CoursClass;
    let coursServiceStub: SinonStubbedInstance<CoursService>;

    beforeEach(() => {
      coursServiceStub = sinon.createStubInstance<CoursService>(CoursService);

      wrapper = shallowMount<CoursClass>(CoursUpdateComponent, {
        store,
        i18n,
        localVue,
        router,
        provide: {
          alertService: () => new AlertService(store),
          coursService: () => coursServiceStub,

          groupService: () => new GroupService(),

          variantService: () => new VariantService(),
        },
      });
      comp = wrapper.vm;
    });

    describe('save', () => {
      it('Should call update service on save for existing entity', async () => {
        // GIVEN
        const entity = { id: '123' };
        comp.cours = entity;
        coursServiceStub.update.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(coursServiceStub.update.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });

      it('Should call create service on save for new entity', async () => {
        // GIVEN
        const entity = {};
        comp.cours = entity;
        coursServiceStub.create.resolves(entity);

        // WHEN
        comp.save();
        await comp.$nextTick();

        // THEN
        expect(coursServiceStub.create.calledWith(entity)).toBeTruthy();
        expect(comp.isSaving).toEqual(false);
      });
    });
  });
});
