package org.gitlab.agitelier.coursgroupes.web.rest;

import org.gitlab.agitelier.coursgroupes.CoursgroupesApp;
import org.gitlab.agitelier.coursgroupes.config.TestSecurityConfiguration;
import org.gitlab.agitelier.coursgroupes.domain.Variant;
import org.gitlab.agitelier.coursgroupes.domain.Workshop;
import org.gitlab.agitelier.coursgroupes.repository.VariantRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link VariantResource} REST controller.
 */
@SpringBootTest(classes = { CoursgroupesApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class VariantResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private VariantRepository variantRepository;

    @Autowired
    private MockMvc restVariantMockMvc;

    private Variant variant;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Variant createEntity() {
        Variant variant = new Variant()
            .name(DEFAULT_NAME)
            .description(DEFAULT_DESCRIPTION);
        // Add required entity
        Workshop workshop;
        workshop = WorkshopResourceIT.createEntity();
        workshop.setId("fixed-id-for-tests");
        variant.setWorkshop(workshop);
        return variant;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Variant createUpdatedEntity() {
        Variant variant = new Variant()
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);
        // Add required entity
        Workshop workshop;
        workshop = WorkshopResourceIT.createUpdatedEntity();
        workshop.setId("fixed-id-for-tests");
        variant.setWorkshop(workshop);
        return variant;
    }

    @BeforeEach
    public void initTest() {
        variantRepository.deleteAll();
        variant = createEntity();
    }

    @Test
    public void createVariant() throws Exception {
        int databaseSizeBeforeCreate = variantRepository.findAll().size();
        // Create the Variant
        restVariantMockMvc.perform(post("/api/variants").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(variant)))
            .andExpect(status().isCreated());

        // Validate the Variant in the database
        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeCreate + 1);
        Variant testVariant = variantList.get(variantList.size() - 1);
        assertThat(testVariant.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testVariant.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createVariantWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = variantRepository.findAll().size();

        // Create the Variant with an existing ID
        variant.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restVariantMockMvc.perform(post("/api/variants").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(variant)))
            .andExpect(status().isBadRequest());

        // Validate the Variant in the database
        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = variantRepository.findAll().size();
        // set the field null
        variant.setName(null);

        // Create the Variant, which fails.


        restVariantMockMvc.perform(post("/api/variants").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(variant)))
            .andExpect(status().isBadRequest());

        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllVariants() throws Exception {
        // Initialize the database
        variantRepository.save(variant);

        // Get all the variantList
        restVariantMockMvc.perform(get("/api/variants?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(variant.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    public void getVariant() throws Exception {
        // Initialize the database
        variantRepository.save(variant);

        // Get the variant
        restVariantMockMvc.perform(get("/api/variants/{id}", variant.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(variant.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }
    @Test
    public void getNonExistingVariant() throws Exception {
        // Get the variant
        restVariantMockMvc.perform(get("/api/variants/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateVariant() throws Exception {
        // Initialize the database
        variantRepository.save(variant);

        int databaseSizeBeforeUpdate = variantRepository.findAll().size();

        // Update the variant
        Variant updatedVariant = variantRepository.findById(variant.getId()).get();
        updatedVariant
            .name(UPDATED_NAME)
            .description(UPDATED_DESCRIPTION);

        restVariantMockMvc.perform(put("/api/variants").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedVariant)))
            .andExpect(status().isOk());

        // Validate the Variant in the database
        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeUpdate);
        Variant testVariant = variantList.get(variantList.size() - 1);
        assertThat(testVariant.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testVariant.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void updateNonExistingVariant() throws Exception {
        int databaseSizeBeforeUpdate = variantRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restVariantMockMvc.perform(put("/api/variants").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(variant)))
            .andExpect(status().isBadRequest());

        // Validate the Variant in the database
        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteVariant() throws Exception {
        // Initialize the database
        variantRepository.save(variant);

        int databaseSizeBeforeDelete = variantRepository.findAll().size();

        // Delete the variant
        restVariantMockMvc.perform(delete("/api/variants/{id}", variant.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Variant> variantList = variantRepository.findAll();
        assertThat(variantList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
