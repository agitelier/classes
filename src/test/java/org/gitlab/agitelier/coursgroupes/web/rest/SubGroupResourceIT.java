package org.gitlab.agitelier.coursgroupes.web.rest;

import org.gitlab.agitelier.coursgroupes.CoursgroupesApp;
import org.gitlab.agitelier.coursgroupes.config.TestSecurityConfiguration;
import org.gitlab.agitelier.coursgroupes.domain.SubGroup;
import org.gitlab.agitelier.coursgroupes.domain.Group;
import org.gitlab.agitelier.coursgroupes.repository.SubGroupRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link SubGroupResource} REST controller.
 */
@SpringBootTest(classes = { CoursgroupesApp.class, TestSecurityConfiguration.class })
@ExtendWith(MockitoExtension.class)
@AutoConfigureMockMvc
@WithMockUser
public class SubGroupResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private SubGroupRepository subGroupRepository;

    @Mock
    private SubGroupRepository subGroupRepositoryMock;

    @Autowired
    private MockMvc restSubGroupMockMvc;

    private SubGroup subGroup;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubGroup createEntity() {
        SubGroup subGroup = new SubGroup()
            .name(DEFAULT_NAME);
        // Add required entity
        Group group;
        group = GroupResourceIT.createEntity();
        group.setId("fixed-id-for-tests");
        subGroup.setGroup(group);
        return subGroup;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static SubGroup createUpdatedEntity() {
        SubGroup subGroup = new SubGroup()
            .name(UPDATED_NAME);
        // Add required entity
        Group group;
        group = GroupResourceIT.createUpdatedEntity();
        group.setId("fixed-id-for-tests");
        subGroup.setGroup(group);
        return subGroup;
    }

    @BeforeEach
    public void initTest() {
        subGroupRepository.deleteAll();
        subGroup = createEntity();
    }

    @Test
    public void createSubGroup() throws Exception {
        int databaseSizeBeforeCreate = subGroupRepository.findAll().size();
        // Create the SubGroup
        restSubGroupMockMvc.perform(post("/api/sub-groups").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subGroup)))
            .andExpect(status().isCreated());

        // Validate the SubGroup in the database
        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeCreate + 1);
        SubGroup testSubGroup = subGroupList.get(subGroupList.size() - 1);
        assertThat(testSubGroup.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    public void createSubGroupWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = subGroupRepository.findAll().size();

        // Create the SubGroup with an existing ID
        subGroup.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restSubGroupMockMvc.perform(post("/api/sub-groups").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subGroup)))
            .andExpect(status().isBadRequest());

        // Validate the SubGroup in the database
        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = subGroupRepository.findAll().size();
        // set the field null
        subGroup.setName(null);

        // Create the SubGroup, which fails.


        restSubGroupMockMvc.perform(post("/api/sub-groups").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subGroup)))
            .andExpect(status().isBadRequest());

        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllSubGroups() throws Exception {
        // Initialize the database
        subGroupRepository.save(subGroup);

        // Get all the subGroupList
        restSubGroupMockMvc.perform(get("/api/sub-groups?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(subGroup.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @SuppressWarnings({"unchecked"})
    public void getAllSubGroupsWithEagerRelationshipsIsEnabled() throws Exception {
        when(subGroupRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restSubGroupMockMvc.perform(get("/api/sub-groups?eagerload=true"))
            .andExpect(status().isOk());

        verify(subGroupRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @SuppressWarnings({"unchecked"})
    public void getAllSubGroupsWithEagerRelationshipsIsNotEnabled() throws Exception {
        when(subGroupRepositoryMock.findAllWithEagerRelationships(any())).thenReturn(new PageImpl(new ArrayList<>()));

        restSubGroupMockMvc.perform(get("/api/sub-groups?eagerload=true"))
            .andExpect(status().isOk());

        verify(subGroupRepositoryMock, times(1)).findAllWithEagerRelationships(any());
    }

    @Test
    public void getSubGroup() throws Exception {
        // Initialize the database
        subGroupRepository.save(subGroup);

        // Get the subGroup
        restSubGroupMockMvc.perform(get("/api/sub-groups/{id}", subGroup.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(subGroup.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    public void getNonExistingSubGroup() throws Exception {
        // Get the subGroup
        restSubGroupMockMvc.perform(get("/api/sub-groups/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateSubGroup() throws Exception {
        // Initialize the database
        subGroupRepository.save(subGroup);

        int databaseSizeBeforeUpdate = subGroupRepository.findAll().size();

        // Update the subGroup
        SubGroup updatedSubGroup = subGroupRepository.findById(subGroup.getId()).get();
        updatedSubGroup
            .name(UPDATED_NAME);

        restSubGroupMockMvc.perform(put("/api/sub-groups").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedSubGroup)))
            .andExpect(status().isOk());

        // Validate the SubGroup in the database
        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeUpdate);
        SubGroup testSubGroup = subGroupList.get(subGroupList.size() - 1);
        assertThat(testSubGroup.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    public void updateNonExistingSubGroup() throws Exception {
        int databaseSizeBeforeUpdate = subGroupRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restSubGroupMockMvc.perform(put("/api/sub-groups").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(subGroup)))
            .andExpect(status().isBadRequest());

        // Validate the SubGroup in the database
        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteSubGroup() throws Exception {
        // Initialize the database
        subGroupRepository.save(subGroup);

        int databaseSizeBeforeDelete = subGroupRepository.findAll().size();

        // Delete the subGroup
        restSubGroupMockMvc.perform(delete("/api/sub-groups/{id}", subGroup.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<SubGroup> subGroupList = subGroupRepository.findAll();
        assertThat(subGroupList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
