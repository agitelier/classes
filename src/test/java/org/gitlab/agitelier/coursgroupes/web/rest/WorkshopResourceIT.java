package org.gitlab.agitelier.coursgroupes.web.rest;

import org.gitlab.agitelier.coursgroupes.CoursgroupesApp;
import org.gitlab.agitelier.coursgroupes.config.TestSecurityConfiguration;
import org.gitlab.agitelier.coursgroupes.domain.Workshop;
import org.gitlab.agitelier.coursgroupes.repository.WorkshopRepository;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link WorkshopResource} REST controller.
 */
@SpringBootTest(classes = { CoursgroupesApp.class, TestSecurityConfiguration.class })
@AutoConfigureMockMvc
@WithMockUser
public class WorkshopResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_URL = "AAAAAAAAAA";
    private static final String UPDATED_URL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private WorkshopRepository workshopRepository;

    @Autowired
    private MockMvc restWorkshopMockMvc;

    private Workshop workshop;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workshop createEntity() {
        Workshop workshop = new Workshop()
            .name(DEFAULT_NAME)
            .url(DEFAULT_URL)
            .description(DEFAULT_DESCRIPTION);
        return workshop;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Workshop createUpdatedEntity() {
        Workshop workshop = new Workshop()
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .description(UPDATED_DESCRIPTION);
        return workshop;
    }

    @BeforeEach
    public void initTest() {
        workshopRepository.deleteAll();
        workshop = createEntity();
    }

    @Test
    public void createWorkshop() throws Exception {
        int databaseSizeBeforeCreate = workshopRepository.findAll().size();
        // Create the Workshop
        restWorkshopMockMvc.perform(post("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workshop)))
            .andExpect(status().isCreated());

        // Validate the Workshop in the database
        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeCreate + 1);
        Workshop testWorkshop = workshopList.get(workshopList.size() - 1);
        assertThat(testWorkshop.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testWorkshop.getUrl()).isEqualTo(DEFAULT_URL);
        assertThat(testWorkshop.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    public void createWorkshopWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = workshopRepository.findAll().size();

        // Create the Workshop with an existing ID
        workshop.setId("existing_id");

        // An entity with an existing ID cannot be created, so this API call must fail
        restWorkshopMockMvc.perform(post("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workshop)))
            .andExpect(status().isBadRequest());

        // Validate the Workshop in the database
        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = workshopRepository.findAll().size();
        // set the field null
        workshop.setName(null);

        // Create the Workshop, which fails.


        restWorkshopMockMvc.perform(post("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workshop)))
            .andExpect(status().isBadRequest());

        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void checkUrlIsRequired() throws Exception {
        int databaseSizeBeforeTest = workshopRepository.findAll().size();
        // set the field null
        workshop.setUrl(null);

        // Create the Workshop, which fails.


        restWorkshopMockMvc.perform(post("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workshop)))
            .andExpect(status().isBadRequest());

        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    public void getAllWorkshops() throws Exception {
        // Initialize the database
        workshopRepository.save(workshop);

        // Get all the workshopList
        restWorkshopMockMvc.perform(get("/api/workshops?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(workshop.getId())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)))
            .andExpect(jsonPath("$.[*].url").value(hasItem(DEFAULT_URL)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())));
    }
    
    @Test
    public void getWorkshop() throws Exception {
        // Initialize the database
        workshopRepository.save(workshop);

        // Get the workshop
        restWorkshopMockMvc.perform(get("/api/workshops/{id}", workshop.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(workshop.getId()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME))
            .andExpect(jsonPath("$.url").value(DEFAULT_URL))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()));
    }
    @Test
    public void getNonExistingWorkshop() throws Exception {
        // Get the workshop
        restWorkshopMockMvc.perform(get("/api/workshops/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    public void updateWorkshop() throws Exception {
        // Initialize the database
        workshopRepository.save(workshop);

        int databaseSizeBeforeUpdate = workshopRepository.findAll().size();

        // Update the workshop
        Workshop updatedWorkshop = workshopRepository.findById(workshop.getId()).get();
        updatedWorkshop
            .name(UPDATED_NAME)
            .url(UPDATED_URL)
            .description(UPDATED_DESCRIPTION);

        restWorkshopMockMvc.perform(put("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedWorkshop)))
            .andExpect(status().isOk());

        // Validate the Workshop in the database
        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeUpdate);
        Workshop testWorkshop = workshopList.get(workshopList.size() - 1);
        assertThat(testWorkshop.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testWorkshop.getUrl()).isEqualTo(UPDATED_URL);
        assertThat(testWorkshop.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    public void updateNonExistingWorkshop() throws Exception {
        int databaseSizeBeforeUpdate = workshopRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restWorkshopMockMvc.perform(put("/api/workshops").with(csrf())
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(workshop)))
            .andExpect(status().isBadRequest());

        // Validate the Workshop in the database
        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    public void deleteWorkshop() throws Exception {
        // Initialize the database
        workshopRepository.save(workshop);

        int databaseSizeBeforeDelete = workshopRepository.findAll().size();

        // Delete the workshop
        restWorkshopMockMvc.perform(delete("/api/workshops/{id}", workshop.getId()).with(csrf())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Workshop> workshopList = workshopRepository.findAll();
        assertThat(workshopList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
