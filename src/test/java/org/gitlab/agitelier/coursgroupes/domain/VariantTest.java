package org.gitlab.agitelier.coursgroupes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.gitlab.agitelier.coursgroupes.web.rest.TestUtil;

public class VariantTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Variant.class);
        Variant variant1 = new Variant();
        variant1.setId("id1");
        Variant variant2 = new Variant();
        variant2.setId(variant1.getId());
        assertThat(variant1).isEqualTo(variant2);
        variant2.setId("id2");
        assertThat(variant1).isNotEqualTo(variant2);
        variant1.setId(null);
        assertThat(variant1).isNotEqualTo(variant2);
    }
}
