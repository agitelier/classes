package org.gitlab.agitelier.coursgroupes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.gitlab.agitelier.coursgroupes.web.rest.TestUtil;

public class WorkshopTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Workshop.class);
        Workshop workshop1 = new Workshop();
        workshop1.setId("id1");
        Workshop workshop2 = new Workshop();
        workshop2.setId(workshop1.getId());
        assertThat(workshop1).isEqualTo(workshop2);
        workshop2.setId("id2");
        assertThat(workshop1).isNotEqualTo(workshop2);
        workshop1.setId(null);
        assertThat(workshop1).isNotEqualTo(workshop2);
    }
}
