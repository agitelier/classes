package org.gitlab.agitelier.coursgroupes.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import org.gitlab.agitelier.coursgroupes.web.rest.TestUtil;

public class SubGroupTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(SubGroup.class);
        SubGroup subGroup1 = new SubGroup();
        subGroup1.setId("id1");
        SubGroup subGroup2 = new SubGroup();
        subGroup2.setId(subGroup1.getId());
        assertThat(subGroup1).isEqualTo(subGroup2);
        subGroup2.setId("id2");
        assertThat(subGroup1).isNotEqualTo(subGroup2);
        subGroup1.setId(null);
        assertThat(subGroup1).isNotEqualTo(subGroup2);
    }
}
