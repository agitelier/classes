package testenv.stepdef;

import io.cucumber.java.en.*;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;


public class testSteps {

    WebDriver driver = null;

    /* ========================================= */
    /* =====   SCENARIO: OUVRIR FIREFOX   ====== */
    /* ========================================= */
    @Given("^User opens firefox$")
    public void open_firefox() throws Throwable{
        String project_path = System.getProperty("user.dir");
        /* Chemin pour Linux : */
        System.setProperty("webdriver.gecko.driver",
            project_path + "/src/test/resources/drivers/linux32_driver/geckodriver");
        /* Chemin pour Windows : 
        System.setProperty("webdriver.gecko.driver",
            project_path + "/src/test/resources/drivers/win32_driver/geckodriver.exe");
         */
        driver = new FirefoxDriver();

        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);

        driver.manage().window().maximize();
    }

    @Given("^User opens login webpage$")
    public void open_login_webpage() throws Throwable{
        driver.navigate().to("http://localhost:9000/");
        driver.findElement(By.id("account-menu__BV_toggle_")).click();
        driver.findElement(By.id("login")).click();
    }

    @Then("^Firefox should be opened$")
    public void firefox_opened() throws Throwable{
        System.out.println("FIREFOX IS OPENED :D");

        driver.close();
    }

    /* ======================================= */
    /* =====   SCENARIO: LOGIN ADMIN   ====== */
    /* ======================================= */
    @When("^User enter a valid admin username and valid password$")
    public void enter_valid_credentials() throws Throwable{
        System.out.println("Enter valid credentials");

        driver.findElement(By.id("username")).sendKeys("admin");
        driver.findElement(By.id("password")).sendKeys("admin");
    }

    @And("^User clicks on login$")
    public void clicks_on_login() throws Throwable{
        System.out.println("Clicks on button");

        driver.findElement(By.id("kc-login")).click();
    }

    @Then("^User should be able to login successfully$")
    public void user_logging_in() throws Throwable{
        System.out.println("The user logged in successfully");

        /* Option 1 */
        driver.findElement(By.id("logout")).isDisplayed();
        /* Option 2 */
        driver.getPageSource().contains("You are logged in as user \"admin\".");
        /* Option 3 */
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:9000/" );

        driver.close();
    }


    /* =============================================== */
    /* =====   SCENARIO: ERRONEOUS CREDENTIALS  ====== */
    /* =============================================== */
    @When("^User enter an erroneous username and password$")
    public void enter_erroneous_credentials() throws Throwable{
        System.out.println("Enter erroneous credentials");

        driver.findElement(By.id("username")).sendKeys("bleep");
        driver.findElement(By.id("password")).sendKeys("bloup");
    }

    @Then("^Error message$")
    public void unsuccessful_login() throws Throwable{
        System.out.println("Login unsuccessful");

        driver.getPageSource().contains("Invalid username or password.");

        driver.close();
    }

    /* =============================================== */
    /* =============================================== */
    /* =============================================== */
    /* ============        STUDENT        ============ */
    /* =============================================== */
    /* =============================================== */
    /* =============================================== */

    /* =============================================== */
    /* =====   SCENARIO: OPEN STUDENT PAGE  ====== */
    /* =============================================== */

    @When("^User click on the student link$")
    public void user_click_student_link() throws Throwable {
        System.out.println("User clicks on student link");
        driver.findElement(By.id("student_page")).click();
    }

    @Then("^User should be on the student page$")
    public void user_on_student_page() throws Throwable{
        System.out.println("User should be on the student page");
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:9000/agitelier/student");
        driver.close();
    }


    /* =============================================== */
    /* =====   SCENARIO: GROUP SELECTION  ====== */
    /* =============================================== */

    @And("^Student select a group$")
    public void student_select_a_group() throws Throwable{
        System.out.println("Student select a group");
        driver.findElement(By.id("g")).click();
    }

    @Then("^Variants should appear$")
    public void variants_should_appear() throws Throwable{
        System.out.println("The variants should appear");
        driver.findElement(By.id("variant-title")).isDisplayed();
        driver.close();
    }


    /* =============================================== */
    /* =============================================== */
    /* =============================================== */
    /* ============        TEACHER        ============ */
    /* =============================================== */
    /* =============================================== */
    /* =============================================== */

    /* =============================================== */
    /* =====   SCENARIO: OPEN TEACHER PAGE  ====== */
    /* =============================================== */

    @When("^User click on the teacher link$")
    public void user_click_teacher_link() throws Throwable {
        System.out.println("User clicks on teacher link");
        driver.findElement(By.id("teacher_page")).click();
    }

    @Then("^User should be on the teacher page$")
    public void user_on_teacher_page() throws Throwable{
        System.out.println("User should be on teacher page");
        String URL = driver.getCurrentUrl();
        Assert.assertEquals(URL, "http://localhost:9000/agitelier/teacher");

        driver.close();
    }


    /* =============================================== */
    /* =====   SCENARIO: TEACHER SELECT A COURSE  ====== */
    /* =============================================== */

    @And("^Teacher select a course$")
    public void teacher_select_course() throws Throwable {
        System.out.println("Teacher select a course");
        driver.findElement(By.id("c")).click();
    }

    @Then("^Course edition should appear$")
    public void course_edition_should_appear() throws Throwable {
        System.out.println("Course edition should appear");
        driver.findElement(By.id("cours-name")).isDisplayed();
        driver.close();
    }


    /* =============================================== */
    /* =====   SCENARIO: TEACHER SELECT A GROUP  ====== */
    /* =============================================== */

    @When("^Teacher create a group")
    public void teacher_create_group() throws Throwable {
        System.out.println("Teacher create a group");
        driver.findElement(By.id("create_btn")).click();
        driver.findElement(By.id("group-number")).sendKeys("96");
        driver.findElement(By.id("save-entity")).click();
    }

    @When("^Teacher select created group$")
    public void teacher_select_created_group() throws Throwable {
        System.out.println("Teacher select created group");
        driver.findElement(By.id("96")).click();
    }

    @When("^Teacher edit a group")
    public void teacher_edit_group() throws Throwable {
        System.out.println("Teacher edit a group");
        driver.findElement(By.id("group-number-display")).click();
        driver.findElement(By.id("group-number")).sendKeys(Keys.CONTROL + "a");
        driver.findElement(By.id("group-number")).sendKeys(Keys.DELETE);
        driver.findElement(By.id("group-number")).sendKeys("76");
        driver.findElement(By.id("save-entity")).click();
    }

    @And("^Teacher select edited group$")
    public void teacher_select_edited_group() throws Throwable {
        System.out.println("Teacher select edited group");
        driver.findElement(By.id("76")).click();
    }

    @Then("^Group edition should appear$")
    public void group_edition_should_appear() throws Throwable {
        System.out.println("Group edition should appear");
        driver.findElement(By.id("group-number-display")).isDisplayed();
        driver.close();
    }

    @Then("^Created group should appear$")
    public void group_created_should_appear() throws Throwable {
        System.out.println("Created group should appear");
        driver.findElement(By.id("group-number-display")).isDisplayed();
        driver.findElement(By.id("group-delete-btn")).click();
        driver.findElement(By.id("group-confirm-delete-btn")).click();
        driver.close();
    }

    @Then("^Edited group should appear$")
    public void group_edited_should_appear() throws Throwable {
        System.out.println("Edited group should appear");
        driver.findElement(By.id("group-number-display")).isDisplayed();
        driver.findElement(By.id("group-delete-btn")).click();
        driver.findElement(By.id("group-confirm-delete-btn")).click();
        driver.close();
    }

    /* =============================================== */
    /* =====   SCENARIO: TEACHER UN-SELECTION    ===== */
    /* =============================================== */

    @When("^Teacher returns home$")
    public void teacher_return_home() throws Throwable {
        System.out.println("Teacher returns home");
        driver.findElement(By.id("breadcrumb-home")).click();
    }

    @When("^Teacher go back in breadcrumb$")
    public void teacher_go_back() throws Throwable {
        System.out.println("Teacher go back in breadcrumb");
        driver.findElement(By.id("breadcrumb-back")).click();
    }

    @Then("^Teacher should be home$")
    public void teacher_should_be_home() throws Throwable {
        System.out.println("Teacher should be home");
        driver.findElement(By.id("page-home")).isDisplayed();
    }
}
