package testenv.runners;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
    features="features",
    glue={"stepdef"},
    format={"pretty", "html:test-output"})
public class testRunner {

}
