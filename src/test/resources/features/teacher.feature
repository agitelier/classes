Feature: Test teacher page functionalities

    Scenario: Go on Teacher page
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the teacher link
        Then User should be on the teacher page

    Scenario: Teacher select a Course
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the teacher link
        And Teacher select a course
        Then Course edition should appear

    Scenario: Teacher create a Group then select it
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the teacher link
        And Teacher select a course
        When Teacher create a group
        And Teacher go back in breadcrumb
        And Teacher select created group
        Then Created group should appear

    Scenario: Teacher edit a Group then select it
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the teacher link
        And Teacher select a course
        When Teacher create a group
        And Teacher go back in breadcrumb
        And Teacher select created group
        Then Teacher edit a group
        And Teacher go back in breadcrumb
        And Teacher select edited group
        Then Edited group should appear

    Scenario: Teacher go home
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the teacher link
        And Teacher select a course
        When Teacher returns home
        Then Teacher should be home
