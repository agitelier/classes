Feature: Test authentification scenarios

    Scenario: Open Firefox
        Given User opens firefox
        And User opens login webpage
        Then Firefox should be opened

    Scenario: Test login with admin credentials
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        Then User should be able to login successfully

    Scenario: Test login with erroneous credentials
        Given User opens firefox
        And User opens login webpage
        When User enter an erroneous username and password
        And User clicks on login
        Then Error message
