Feature: Test student page functionalities

    Scenario: Go on Student page
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the student link
        Then User should be on the student page

    Scenario: Student select a Group
        Given User opens firefox
        And User opens login webpage
        When User enter a valid admin username and valid password
        And User clicks on login
        When User click on the student link
        And Student select a group
        Then Variants should appear
