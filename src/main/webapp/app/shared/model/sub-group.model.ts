import { IStudent } from '@/shared/model/student.model';
import { IGroup } from '@/shared/model/group.model';

export interface ISubGroup {
  id?: string;
  name?: string;
  students?: IStudent[];
  group?: IGroup;
}

export class SubGroup implements ISubGroup {
  constructor(public id?: string, public name?: string, public students?: IStudent[], public group?: IGroup) {}
}
