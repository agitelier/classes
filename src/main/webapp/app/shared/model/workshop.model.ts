import { IVariant } from '@/shared/model/variant.model';

export interface IWorkshop {
  id?: string;
  name?: string;
  url?: string;
  description?: any;
  variants?: IVariant[];
}

export class Workshop implements IWorkshop {
  constructor(public id?: string, public name?: string, public url?: string, public description?: any, public variants?: IVariant[]) {}
}
