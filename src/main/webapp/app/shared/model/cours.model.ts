import { IGroup } from '@/shared/model/group.model';
import { IVariant } from '@/shared/model/variant.model';

export interface ICours {
  id?: string;
  symbol?: string;
  name?: string;
  description?: any;
  groups?: IGroup[];
  variants?: IVariant[];
}

export class Cours implements ICours {
  constructor(
    public id?: string,
    public symbol?: string,
    public name?: string,
    public description?: any,
    public groups?: IGroup[],
    public variants?: IVariant[]
  ) {}
}
