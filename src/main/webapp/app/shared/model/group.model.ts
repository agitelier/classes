import { ISubGroup } from '@/shared/model/sub-group.model';
import { IStudent } from '@/shared/model/student.model';
import { ICours } from '@/shared/model/cours.model';

export interface IGroup {
  id?: string;
  number?: number;
  teacherID?: string;
  description?: any;
  subGroups?: ISubGroup[];
  students?: IStudent[];
  cours?: ICours;
}

export class Group implements IGroup {
  constructor(
    public id?: string,
    public number?: number,
    public teacherID?: string,
    public description?: any,
    public subGroups?: ISubGroup[],
    public students?: IStudent[],
    public cours?: ICours
  ) {}

  public addSubGroup(sub) {
    this.subGroups.push(sub);
  }
}
