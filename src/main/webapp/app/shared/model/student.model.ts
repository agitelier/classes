import { IGroup } from '@/shared/model/group.model';
import { ISubGroup } from '@/shared/model/sub-group.model';

export interface IStudent {
  id?: string;
  email?: string;
  groups?: IGroup[];
  subgroups?: ISubGroup[];
}

export class Student implements IStudent {
  constructor(public id?: string, public email?: string, public groups?: IGroup[], public subgroups?: ISubGroup[]) {}
}
