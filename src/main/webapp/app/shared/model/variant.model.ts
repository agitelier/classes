import { IWorkshop } from '@/shared/model/workshop.model';
import { ICours } from '@/shared/model/cours.model';

export interface IVariant {
  id?: string;
  name?: string;
  description?: any;
  workshop?: IWorkshop;
  cours?: ICours[];
}

export class Variant implements IVariant {
  constructor(public id?: string, public name?: string, public description?: any, public workshop?: IWorkshop, public cours?: ICours[]) {}
}
