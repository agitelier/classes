import { Authority } from '@/shared/security/authority';
/* tslint:disable */
// prettier-ignore

// prettier-ignore
const Cours = () => import('@/entities/cours/cours.vue');
// prettier-ignore
const CoursUpdate = () => import('@/entities/cours/cours-update.vue');
// prettier-ignore
const CoursDetails = () => import('@/entities/cours/cours-details.vue');
// prettier-ignore
const Variant = () => import('@/entities/variant/variant.vue');
// prettier-ignore
const VariantUpdate = () => import('@/entities/variant/variant-update.vue');
// prettier-ignore
const VariantDetails = () => import('@/entities/variant/variant-details.vue');
// prettier-ignore
const Group = () => import('@/entities/group/group.vue');
// prettier-ignore
const GroupUpdate = () => import('@/entities/group/group-update.vue');
// prettier-ignore
const GroupDetails = () => import('@/entities/group/group-details.vue');
// prettier-ignore
const SubGroup = () => import('@/entities/sub-group/sub-group.vue');
// prettier-ignore
const SubGroupUpdate = () => import('@/entities/sub-group/sub-group-update.vue');
// prettier-ignore
const SubGroupDetails = () => import('@/entities/sub-group/sub-group-details.vue');
// prettier-ignore
const Workshop = () => import('@/entities/workshop/workshop.vue');
// prettier-ignore
const WorkshopUpdate = () => import('@/entities/workshop/workshop-update.vue');
// prettier-ignore
const WorkshopDetails = () => import('@/entities/workshop/workshop-details.vue');
// prettier-ignore
const Student = () => import('@/entities/student/student.vue');
// prettier-ignore
const StudentUpdate = () => import('@/entities/student/student-update.vue');
// prettier-ignore
const StudentDetails = () => import('@/entities/student/student-details.vue');
// jhipster-needle-add-entity-to-router-import - JHipster will import entities to the router here

const Teacher_View = () => import('@/agitelier/teacher_view/teacher.vue');

const Student_View = () => import('@/agitelier/student_view/student.vue');

export default [
  {
    path: '/gateway/:workshopName/:workshopName/:variantId/:subGroupId',
    name: 'LienVariant',
  },
  {
    path: '/agitelier/teacher',
    name: 'TeacherInterface',
    component: Teacher_View,
  },
  {
    path: '/agitelier/student',
    name: 'StudentInterface',
    component: Student_View,
  },
  {
    path: '/variant/:variantId/view',
    name: 'VariantView',
    component: VariantDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group',
    name: 'Group',
    component: Group,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/new',
    name: 'GroupCreate',
    component: GroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/:groupId/edit',
    name: 'GroupEdit',
    component: GroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/group/:groupId/view',
    name: 'GroupView',
    component: GroupDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sub-group',
    name: 'SubGroup',
    component: SubGroup,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sub-group/new',
    name: 'SubGroupCreate',
    component: SubGroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sub-group/:subGroupId/edit',
    name: 'SubGroupEdit',
    component: SubGroupUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/sub-group/:subGroupId/view',
    name: 'SubGroupView',
    component: SubGroupDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/workshop',
    name: 'Workshop',
    component: Workshop,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/workshop/new',
    name: 'WorkshopCreate',
    component: WorkshopUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/workshop/:workshopId/edit',
    name: 'WorkshopEdit',
    component: WorkshopUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/workshop/:workshopId/view',
    name: 'WorkshopView',
    component: WorkshopDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/student',
    name: 'Student',
    component: Student,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/student/new',
    name: 'StudentCreate',
    component: StudentUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/student/:studentId/edit',
    name: 'StudentEdit',
    component: StudentUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/student/:studentId/view',
    name: 'StudentView',
    component: StudentDetails,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cours/new',
    name: 'CoursCreate',
    component: CoursUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cours/:coursId/edit',
    name: 'CoursEdit',
    component: CoursUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/variant',
    name: 'Variant',
    component: Variant,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/variant/new',
    name: 'VariantCreate',
    component: VariantUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/variant/:variantId/edit',
    name: 'VariantEdit',
    component: VariantUpdate,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cours',
    name: 'Cours',
    component: Cours,
    meta: { authorities: [Authority.USER] },
  },
  {
    path: '/cours/:coursId/view',
    name: 'CoursView',
    component: CoursDetails,
    meta: { authorities: [Authority.USER] },
  },
  // jhipster-needle-add-entity-to-router - JHipster will add entities to the router here
];
