import { Component, Inject, Prop, Watch } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import Vue2Filters from 'vue2-filters';
import AlertMixin from '@/shared/alert/alert.mixin';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import AlertService from '@/shared/alert/alert.service';
import GroupService from '@/entities/group/group.service';
import CoursService from '@/entities/cours/cours.service';
import SubGroupService from '@/entities/sub-group/sub-group.service';
import StudentService from '@/entities/student/student.service';
import { IGroup, Group } from '@/shared/model/group.model';
import { IStudent } from '@/shared/model/student.model';
import { ISubGroup } from '@/shared/model/sub-group.model';
import { ICours } from '@/shared/model/cours.model';

const validations: any = {
  group: {
    number: {
      required,
      numeric,
      min: minValue(0),
      max: maxValue(99),
    },
    teacherID: {
      required,
      maxLength: maxLength(70),
    },
    description: {},
    cours: {
      required,
    },
  },
};

@Component({
  validations,
  mixins: [Vue2Filters.mixin],
})
export default class Group_View extends mixins(AlertMixin) {
  @Inject('groupService') private groupService: () => GroupService;

  @Prop() group!: IGroup;
  @Prop() cours!: ICours;

  @Inject('subGroupService') private subGroupService: () => SubGroupService;

  public subGroups: ISubGroup[] = [];

  @Inject('studentService') private studentService: () => StudentService;

  public students: IStudent[] = [];

  @Inject('coursService') private coursService: () => CoursService;

  public isSaving = false;
  public currentLanguage = '';
  public addStudents = false;
  public numSelected = false;
  public descSelected = false;
  public creationMode = false;
  public modifStudent = false;
  public NUMBER = 'n';
  public DESCRIPTION = 'd';

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    if (this.group === null) {
      this.creationMode = true;
      this.group = new Group();
      this.group.number = null;
      this.group.description = null;
      this.group.teacherID = this.$store.getters.account.login;
      this.group.cours = this.cours;
      this.group.students = [];
      this.retrieveStudents();
    }
    this.alertService().initAlert();
    this.getAlertFromStore();
  }

  beforeUpdate(): void {
    if (this.group === null) {
      this.creationMode = true;
      this.group = new Group();
      this.group.number = null;
      this.group.description = null;
      this.group.teacherID = this.$store.getters.account.login;
      this.group.cours = this.cours;
      this.group.students = [];
      this.retrieveStudents();
    }
  }
  public selected(item) {
    if (item === this.NUMBER) {
      this.numSelected = true;
    } else if (item === this.DESCRIPTION) {
      this.descSelected = true;
    }
  }

  public save(): void {
    this.isSaving = true;
    if (this.group.id) {
      this.groupService()
        .update(this.group)
        .then(
          param => {
            this.isSaving = false;
            const message = this.$t('coursgroupesApp.group.updated', { param: param.id });
            this.alertService().showAlert(message, 'info');
            this.getAlertFromStore();
            this.numSelected = false;
            this.descSelected = false;
            this.modifStudent = false;
          },
          error => {
            this.isSaving = false;
            console.log('error:' + error);
            const message = this.$t('coursgroupesApp.group.rejected', {}) + ' ' + this.$t('coursgroupesApp.group.errorDuplicated', {});
            this.alertService().showAlert(message, 'danger');
            this.getAlertFromStore();
          }
        );
    } else {
      this.groupService()
        .create(this.group)
        .then(
          param => {
            this.isSaving = false;
            const message = this.$t('coursgroupesApp.group.created', { param: param.id });
            this.alertService().showAlert(message, 'success');
            this.getAlertFromStore();
            this.creationMode = false;
            this.$parent.loadGroups();
            this.$parent.selection('g', param);
            this.$emit('save');
          },
          error => {
            this.isSaving = false;
            console.log('error:' + error);
            const message = this.$t('coursgroupesApp.group.rejected', {}) + ' ' + this.$t('coursgroupesApp.group.errorDuplicated', {});
            this.alertService().showAlert(message, 'danger');
            this.getAlertFromStore();
          }
        );
    }
  }

  public reload(): void {
    this.groupService()
      .find(this.group.id)
      .then(res => {
        this.group.number = res.number;
        this.group.description = res.description;
        this.group.students = res.students;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  prepareDeleteGroup(): void {
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public retrieveStudents() {
    this.studentService()
      .retrieve()
      .then(res => {
        this.students = res.data;
      });
  }

  public prepareModification(): void {
    this.modifStudent = true;
    this.retrieveStudents();
  }

  public removeGroup() {
    this.groupService()
      .delete(this.group.id)
      .then(() => {
        const message = this.$t('coursgroupesApp.group.deleted', { param: this.group.id });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.closeDialog();
      });
    this.$emit('remove');
  }

  public previousState(): void {
    if (this.creationMode) {
      this.creationMode = false;
      this.group = null;
      this.$emit('remove');
    } else {
      this.reload();
      this.numSelected = false;
      this.descSelected = false;
      this.modifStudent = false;
      this.$emit('cancel');
    }
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
