import { Component, Inject, Prop } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';
import AlertMixin from '@/shared/alert/alert.mixin';

import AlertService from '@/shared/alert/alert.service';
import VariantService from '@/entities/variant/variant.service';
import { IVariant } from '@/shared/model/variant.model';
import WorkshopService from '@/entities/workshop/workshop.service';
import { IWorkshop } from '@/shared/model/workshop.model';

@Component
export default class Variants_View extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('variantService') private variantService: () => VariantService;
  @Inject('workshopService') private workshopService: () => WorkshopService;

  @Prop() variants!: IVariant[];
  @Prop() type!: string;

  public currentLanguage = '';
  public workshop: IWorkshop;
  public workshopName = '';

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  mounted(): void {
    this.workshopName = this.findName(this.type);
    this.findWorkshop();
  }

  public save(instance: IVariant): void {
    instance.workshop = this.workshop;
    if (instance.name === undefined) {
      instance.name = instance.id;
    }
    instance.id = null;
    this.variantService()
      .create(instance)
      .then(() => {
        const message = this.$t('coursgroupesApp.variants.created', { param: instance.name });
        this.alertService().showAlert(message, 'success');
      })
      .catch(err => {
        const message = this.$t('Nom de variant existant', { param: instance.name });
        this.alertService().showAlert(message, 'danger');
        //this.getAlertFromStore();
      });
  }

  public findWorkshop(): void {
    this.workshopService()
      .findByName(this.workshopName)
      .then(res => {
        this.workshop = res;
        console.log(res.name);
      });
  }

  public findName(type): string {
    let name = '';
    if (type === 'CALCFORMS') {
      name = 'Formulaire de calcul';
    } else if (type === 'VALUESTREAM') {
      name = 'Chaîne de valeur';
    } else if (type === 'QUIZ') {
      name = 'Quiz';
    } else {
      name = 'Formulaire';
    }
    return name;
  }
}
