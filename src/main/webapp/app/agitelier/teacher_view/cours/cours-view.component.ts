import { Component, Inject, Prop, Vue } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';
import Vue2Filters from 'vue2-filters';
import AlertMixin from '@/shared/alert/alert.mixin';
import VariantsView from '../variants/variant-view.vue';

import VariantService from '@/entities/variant/variant.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';
import { IVariant } from '@/shared/model/variant.model';

//import AlertService from '@/shared/alert/alert.service';

const validations: any = {
  cours: {
    symbol: {
      required,
      maxLength: maxLength(10),
    },
    name: {
      required,
      maxLength: maxLength(70),
    },
    description: {},
  },
};

Vue.component('VariantsView', VariantsView);

@Component({
  validations,
  mixins: [Vue2Filters.mixin],
})
export default class Cours_View extends mixins(AlertMixin) {
  //@Inject('alertService') private alertService: () => AlertService;
  @Inject('coursService') private coursService: () => CoursService;
  @Inject('groupService') private groupService: () => GroupService;
  @Inject('variantService') private variantService: () => VariantService;
  @Prop() cours!: ICours;

  public groups: IGroup[] = [];
  public variants: IVariant[] = [];
  public variantsList: IVariant[] = [];
  public isSaving = false;
  public currentLanguage = '';
  private removeId: string = null;
  public selectedWorkshop = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      vm.initRelationships();
    });
  }

  beforeUpdate() {
    if (!this.$parent.coursIsEditing) {
      this.$parent.coursIsEditing = true;
    }
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.cours.variants = [];
    this.initRelationships();
    this.alertService().initAlert();
    this.getAlertFromStore();
    this.variantsList = [];
  }

  public previousState(): void {
    if (this.$parent.coursIsEditing) {
      Object.assign(this.cours, this.$parent.selectedCoursBeforeEdit);
      this.$parent.coursIsEditing = false;
    }
    this.$emit('cancel');
  }

  public showVariants(type) {
    this.selectedWorkshop = type;
    this.retrieveVariantsList(this.selectedWorkshop);
    if (<any>this.$refs.variantsList) {
      (<any>this.$refs.variantsList).show();
    }
  }

  public hideVariants() {
    (<any>this.$refs.variantsList).hide();
  }

  public retrieveVariantsList(variantType): void {
    if (variantType === 'CALCFORMS') {
      this.variantService()
        .retrieveVariantsCalcforms()
        .then(res => {
          this.variantsList = res.data;
        });
    } else if (variantType === 'VALUESTREAM') {
      this.variantService()
        .retrieveVariantsValueStream()
        .then(res => {
          this.variantsList = res.data;
        });
    }
  }

  public save(): void {
    this.isSaving = true;

    if (this.cours.id) {
      this.coursService()
        .update(this.cours)
        .then(
          param => {
            this.isSaving = false;
            const message = this.$t('coursgroupesApp.cours.updated', { param: param.id });
            this.alertService().showAlert(message, 'info');
            this.getAlertFromStore();
          },
          error => {
            this.isSaving = false;
            console.log('error:' + error);
            const message = this.$t('coursgroupesApp.cours.rejected', {}) + ' ' + this.$t('coursgroupesApp.cours.errorDuplicated', {});

            this.alertService().showAlert(message, 'danger');
            this.getAlertFromStore();
          }
        );
    } else {
      this.coursService()
        .create(this.cours)
        .then(
          param => {
            const message = this.$t('coursgroupesApp.cours.created', { param: this.cours.name });
            this.alertService().showAlert(message, 'success');
            this.getAlertFromStore();
            this.isSaving = false;
            this.$parent.loadCours();
            this.$parent.selection('c', param);
            this.$emit('save');
          },
          error => {
            this.isSaving = false;
            console.log('error:' + error);
            const message = this.$t('coursgroupesApp.cours.rejected', {}) + ' ' + this.$t('coursgroupesApp.cours.errorDuplicated', {});

            this.alertService().showAlert(message, 'danger');
            this.getAlertFromStore();
          }
        );
    }
  }

  public retrieveCours(coursId): void {
    this.coursService()
      .find(coursId)
      .then(res => {
        this.cours = res;
      });
  }

  public initRelationships(): void {
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
    this.variantService()
      .retrieve()
      .then(res => {
        this.variants = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }

  public prepareRemove(instance: ICours): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCours(): void {
    this.coursService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.cours.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.closeDialog();
        this.$emit('remove');
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
