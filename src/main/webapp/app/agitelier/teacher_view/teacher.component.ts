import Component from 'vue-class-component';
import { Vue, Inject, Component } from 'vue-property-decorator';

import TableTest from './table-test.vue';
import MyView from './view.vue';
import CoursView from './cours/cours-view.vue';
import SubGroupView from './subgroup/sub-group.vue';
import GroupView from './group/group-view.vue';

import { Group, IGroup } from '@/shared/model/group.model';
import { SubGroup } from '@/shared/model/sub-group.model';
import { Cours } from '@/shared/model/cours.model';
import CoursService from '@/entities/cours/cours.service';
import GroupService from '@/entities/group/group.service';
import SubGroupService from './subgroup/sub-group.service';

export default {
  components: {
    TableTest,
    MyView,
    CoursView,
    SubGroupView,
    GroupView,
  },
  data() {
    return {
      selectedGroup: null,
      selectedCours: null,
      selectedCoursBeforeEdit: null,
      coursIsEditing: false,
      selectedSub: null,
      coursList: [],
      groups: [],
      subGroups: [],
      isCreating: false,
      GROUP: 'g',
      SUBGROUP: 's',
      COURS: 'c',
      newCours: null,
      creatingGroup: false,
    };
  },
  computed: {
    teacherID: {
      get: function () {
        return this.$store.getters.account ? this.$store.getters.account.login : '';
      },
    },
  },
  methods: {
    selection(what, value) {
      this.creating(null);

      if (what === this.GROUP) {
        this.creatingGroup = false;
        this.selectedGroup = value;
        this.loadSubGroups();
        this.selectedSub = null;
      } else if (what === this.SUBGROUP) {
        this.selectedSub = value;
      } else if (what === this.COURS) {
        this.selectedCours = value;
        this.selectedCoursBeforeEdit = Object.assign({}, value);
        this.selectedGroup = null;
        this.selectedSub = null;
        this.subGroups = null;
        this.loadGroups();
      }
    },
    creating(what) {
      if (what === null) {
        this.isCreating = false;
        this.creatingGroup = false;
        return;
      }
      this.isCreating = true;
      if (what === this.COURS) {
        this.initializeSelected();
        const coursService = new CoursService();
        this.newCours = coursService.newCours;
        this.selectedCours = this.newCours;
        this.groups = null;
      } else if (what === this.GROUP) {
        this.creatingGroup = true;
        this.selectedGroup = null;
        this.selectedSub = null;
      } else if (what === this.SUBGROUP) {
        let subGroup: SubGroup = new SubGroup();
        subGroup.id = null;
        subGroup.students = [];
        subGroup.group = this.selectedGroup;
        this.selection(this.SUBGROUP, subGroup);
      }
    },
    canceling(what) {
      if (what === this.COURS) {
        if (this.isCreating) {
          this.isCreating = false;
          this.initializeSelected();
          this.loadCours();
        }
      } else if (what === this.GROUP) {
        this.creatingGroup = false;
      } else if (what === this.SUBGROUP) {
        //this.selection(this.SUBGROUP, null);
      }
    },
    removing(what) {
      if (what === this.COURS) {
        this.initializeSelected();
        this.loadCours();
        this.groups = null;
      } else if (what === this.GROUP) {
        this.creatingGroup = false;
        this.selectedGroup = null;
        this.selectedSub = null;
        this.loadGroups();
      } else if (what === this.SUBGROUP) {
        //this.selection(this.SUBGROUP, null);
      }
    },
    saving(what) {
      if (what === this.COURS) {
        this.isCreating = false;
      } else if (what === this.GROUP) {
        this.creatingGroup = false;
      } else if (what === this.SUBGROUP) {
      }
    },
    initializeSelected() {
      this.selectedCours = null;
      this.selectedCoursBeforeEdit = Object.assign({}, null);
      this.selectedGroup = null;
      this.selectedSub = null;
    },
    loadCours() {
      const coursService = new CoursService();

      coursService.retrieve().then(
        res => {
          this.coursList = res.data;
          res.data.forEach(function (value) {
            console.log(value.name);
          });
        },
        err => {
          console.log('error loadcours');
          console.log(err);
        }
      );
    },
    loadSubGroups() {
      const subGroupService = new SubGroupService();

      subGroupService.retrieveByGroup(this.selectedGroup.id).then(
        res => {
          this.subGroups = res.data;

          if (this.selectedSub != null && this.subGroups != null) {
            for (let sg of this.subGroups) {
              if (sg.id == this.selectedSub.id) {
                this.selectedSub = sg;
                break;
              }
            }
          }
        },
        err => {
          console.log('Retrieval of Sub-Groups failed');
          console.log(err);
        }
      );
    },
    loadGroups() {
      const groupService = new GroupService();

      if (this.teacherID !== '') {
        groupService.findForTeacherAndCours(this.teacherID, this.selectedCours.id).then(
          res => {
            this.groups = res.data;
          },
          err => {
            console.log('Retrieval of Groups failed');
            console.log(err);
          }
        );
      } else {
        groupService.retrieve().then(
          res => {
            this.groups = res.data;
            console.log(this.groups);
          },
          err => {
            console.log('Retrieval of Groups failed');
            console.log(err);
          }
        );
      }
    },
  },
  mounted() {
    this.loadCours();
  },
};
