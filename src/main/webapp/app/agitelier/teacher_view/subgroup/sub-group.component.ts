import { mixins } from 'vue-class-component';

import { Component, Vue, Inject, Prop } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import AlertMixin from '@/shared/alert/alert.mixin';

import SubGroupService from './sub-group.service';
import StudentService from '@/entities/student/student.service';
import GroupService from '@/entities/group/group.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class SubGroup extends mixins(AlertMixin) {
  @Inject('subGroupService') private subGroupService: () => SubGroupService;
  @Inject('studentService') private studentService: () => StudentService;
  @Inject('groupService') private groupService: () => GroupService;
  @Prop() obj!: ISubGroup;

  private removeId: string = null;
  public renameName: string = null;
  private renameId: string = null;

  public students: IStudent[] = [];

  //private subGroup: ISubGroup;
  //private subGroups: ISubGroup[] = [];
  //private groups: ISubGroup[] = [];

  public isFetching = false;

  public mounted(): void {}

  public clear(): void {}

  created(): void {
    this.alertService().initAlert();
    this.getAlertFromStore();
  }

  public save(): void {
    this.closeDialog();
    this.isSaving = true;

    if (this.obj.id) {
      this.subGroupService()
        .update(this.obj)
        .then(param => {
          this.isSaving = false;

          const message = this.$t('coursgroupesApp.subGroup.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
          this.getAlertFromStore();
          this.$parent.selection('s', param);
        });
    } else {
      this.subGroupService()
        .create(this.obj)
        .then(param => {
          this.isSaving = false;

          const message = this.$t('coursgroupesApp.subGroup.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
          this.getAlertFromStore();
          this.$parent.loadSubGroups();
          this.$parent.selection('s', param);
        });
    }
  }

  public reload(): void {
    this.subGroupService()
      .find(this.obj.id)
      .then(res => {
        this.obj.name = res.name;
        this.obj.students = res.students;
        const message = this.$t('coursgroupesApp.subGroup.reload', { param: res.name });
        this.alertService().showAlert(message, 'success');
        this.getAlertFromStore();
      });
  }

  public prepareRemove(instance: ISubGroup): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public prepareRename(instance: ISubGroup): void {
    this.renameId = instance.id;
    this.renameName = instance.name;
    if (<any>this.$refs.renameEntity) {
      (<any>this.$refs.renameEntity).show();
    }
  }

  public renameSubGroup(): void {
    this.subGroupService()
      .find(this.renameId)
      .then(res => {
        let subGroup: ISubGroup = new SubGroup();
        subGroup = res;
        subGroup.name = this.renameName;
        this.subGroupService()
          .update(subGroup)
          .then(() => {
            this.renameId = null;
            //this.retrieveObj();
            this.obj.name = this.renameName;
            this.closeDialog();
          });
      });
  }

  public removeSubGroup(): void {
    this.subGroupService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.subGroup.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.closeDialog();

        this.$parent.selection('g', this.obj.group);
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
    (<any>this.$refs.renameEntity).hide();
    (<any>this.$refs.changeStudents).hide();
  }

  public retrieveGroup(): void {
    this.groupService()
      .find(this.obj.group.id)
      .then(res => {
        this.obj.group = res;
        this.students = this.obj.group.students;
      });
  }

  public prepareChangeStudents(): void {
    this.retrieveGroup();
    (<any>this.$refs.changeStudents).show();
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
