import CoursView from './components/student-cours-details.vue';
import VariantView from './components/variant.vue';
import TableTest from '../teacher_view/table-test.vue';
import CoursService from '@/entities/cours/cours.service';
import GroupService from '@/entities/group/group.service';

export default {
  components: {
    TableTest,
    CoursView,
    VariantView,
  },
  data() {
    return {
      selectedGroup: null,
      selectedCours: null,
      selectedSub: null,
      groupsList: [],
      coursList: [],
      groups: [],
      tabIndex: 0,
      service: null,
      GROUP: 'g',
    };
  },
  computed: {
    accountID: {
      get: function () {
        return this.$store.getters.account ? this.$store.getters.account.login : '';
      },
    },
    complexGroup: {
      get: function () {
        return this.selectedGroup;
      },
    },
  },
  methods: {
    selection(what, value) {
      if (what === this.GROUP) {
        this.selectedGroup = value;
        this.selectedSub = null;
      }
    },
    resetUI(newTabIndex) {
      this.tabIndex = newTabIndex;
      this.selectedCours = null;
      this.selectedGroup = null;
      this.selectedSub = null;
      this.service = null;
    },
    loadGroups() {
      this.selection(this.GROUP, null);
      const groupService = new GroupService();

      console.log('Current Account Login : ' + this.accountID);
      if (this.accountID !== '') {
        groupService.findForStudent(this.accountID).then(
          res => {
            this.groupsList = res.data;
            console.log(this.groupsList);
          },
          err => {
            console.log(err);
          }
        );
      }
      groupService.retrieve().then(
        res => {
          this.groupsList = res.data;
          console.log(this.groupsList);
        },
        err => {
          console.log(err);
        }
      );
    },
    loadcours() {
      const coursService = new CoursService();

      coursService.retrieve().then(
        res => {
          res.data.forEach(function (value) {
            console.log(value.name);
          });
          this.coursList = res.data;
        },
        err => {}
      );
    },
  },
  mounted() {
    this.loadcours();
    this.loadGroups();
  },
};
