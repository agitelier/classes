import { mixins } from 'vue-class-component';

import { Component, Inject, Prop } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IVariant } from '@/shared/model/variant.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import JhiDataUtils from '@/shared/data/data-utils.service';

import VariantService from './variant.service';
import { ISubGroup } from '@/shared/model/sub-group.model';
import { IGroup } from '@/shared/model/group.model';
import SubGroupService from '@/entities/sub-group/sub-group.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Variant extends mixins(JhiDataUtils, AlertMixin) {
  @Prop() variants!: IVariant[];
  @Prop() group!: IGroup;
  @Prop() subgroup!: ISubGroup;
  @Prop() accountID;
  @Inject('variantService') private variantService: () => VariantService;

  public getSubGroupID(): String {
    const subGroupService = new SubGroupService();
    console.log('Current student id : ' + this.accountID);
    console.log('Current group id : ' + this.group.id);
    if (this.accountID !== '') {
      if (this.group.id !== '') {
        subGroupService.findForStudentAndGroup(this.accountID, this.group.id).then(
          res => {
            this.subgroup = res.data;
            console.log(this.subgroup);
            if (this.subgroup != null) return this.subgroup.id;
          },
          err => {
            console.log(err);
            return '0';
          }
        );
      }
      return '0';
    }
  }
}
