import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import SubGroupService from '../sub-group/sub-group.service';
import { ISubGroup } from '@/shared/model/sub-group.model';

import StudentService from '../student/student.service';
import { IStudent } from '@/shared/model/student.model';

import CoursService from '../cours/cours.service';
import { ICours } from '@/shared/model/cours.model';

import AlertService from '@/shared/alert/alert.service';
import { IGroup, Group } from '@/shared/model/group.model';
import GroupService from './group.service';

const validations: any = {
  group: {
    number: {
      required,
      numeric,
      min: minValue(0),
      max: maxValue(99),
    },
    teacherID: {
      required,
      maxLength: maxLength(70),
    },
    description: {},
    cours: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class GroupUpdate extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('groupService') private groupService: () => GroupService;
  public group: IGroup = new Group();

  @Inject('subGroupService') private subGroupService: () => SubGroupService;

  public subGroups: ISubGroup[] = [];

  @Inject('studentService') private studentService: () => StudentService;

  public students: IStudent[] = [];

  @Inject('coursService') private coursService: () => CoursService;

  public cours: ICours[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.groupId) {
        vm.retrieveGroup(to.params.groupId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.group.students = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.group.id) {
      this.groupService()
        .update(this.group)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.group.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        })
        .catch(err => {
          this.alertService().showAlert(err, 'info');
        });
    } else {
      this.groupService()
        .create(this.group)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.group.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        })
        .catch(err => {
          const message = this.$t('coursgroupesApp.group.rejected', { err });
          this.alertService().showAlert(message, 'info');
        });
    }
  }

  public retrieveGroup(groupId): void {
    this.groupService()
      .find(groupId)
      .then(res => {
        this.group = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.subGroupService()
      .retrieve()
      .then(res => {
        this.subGroups = res.data;
      });
    this.studentService()
      .retrieve()
      .then(res => {
        this.students = res.data;
      });
    this.coursService()
      .retrieve()
      .then(res => {
        this.cours = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
