import axios from 'axios';

import { IGroup } from '@/shared/model/group.model';

const baseApiUrl = 'api/groups';

const teacherApiUrl = 'api/groups-by-teacher-and-cours';

const coursApiUrl = 'api/groups-by-cours';

const studentApiUrl = 'api/groups-by-student';

export default class GroupService {
  public find(id: string): Promise<IGroup> {
    return new Promise<IGroup>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findForStudent(StudentID: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${studentApiUrl}/${StudentID}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findForTeacherAndCours(TeacherID: string, coursId: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${teacherApiUrl}/teacherID=${TeacherID}&coursId=${coursId}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findForCours(coursId: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${coursApiUrl}/${coursId}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IGroup): Promise<IGroup> {
    return new Promise<IGroup>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IGroup): Promise<IGroup> {
    return new Promise<IGroup>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
