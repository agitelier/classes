import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IWorkshop } from '@/shared/model/workshop.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import JhiDataUtils from '@/shared/data/data-utils.service';

import WorkshopService from './workshop.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Workshop extends mixins(JhiDataUtils, AlertMixin) {
  @Inject('workshopService') private workshopService: () => WorkshopService;
  private removeId: string = null;

  public workshops: IWorkshop[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllWorkshops();
  }

  public clear(): void {
    this.retrieveAllWorkshops();
  }

  public retrieveAllWorkshops(): void {
    this.isFetching = true;

    this.workshopService()
      .retrieve()
      .then(
        res => {
          this.workshops = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IWorkshop): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeWorkshop(): void {
    this.workshopService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.workshop.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllWorkshops();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
