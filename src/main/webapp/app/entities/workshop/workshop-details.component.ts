import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IWorkshop } from '@/shared/model/workshop.model';
import WorkshopService from './workshop.service';

@Component
export default class WorkshopDetails extends mixins(JhiDataUtils) {
  @Inject('workshopService') private workshopService: () => WorkshopService;
  public workshop: IWorkshop = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.workshopId) {
        vm.retrieveWorkshop(to.params.workshopId);
      }
    });
  }

  public retrieveWorkshop(workshopId) {
    this.workshopService()
      .find(workshopId)
      .then(res => {
        this.workshop = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
