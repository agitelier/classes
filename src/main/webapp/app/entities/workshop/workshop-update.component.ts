import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import VariantService from '../variant/variant.service';
import { IVariant } from '@/shared/model/variant.model';

import AlertService from '@/shared/alert/alert.service';
import { IWorkshop, Workshop } from '@/shared/model/workshop.model';
import WorkshopService from './workshop.service';

const validations: any = {
  workshop: {
    name: {
      required,
      maxLength: maxLength(30),
    },
    url: {
      required,
    },
    description: {},
  },
};

@Component({
  validations,
})
export default class WorkshopUpdate extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('workshopService') private workshopService: () => WorkshopService;
  public workshop: IWorkshop = new Workshop();

  @Inject('variantService') private variantService: () => VariantService;

  public variants: IVariant[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.workshopId) {
        vm.retrieveWorkshop(to.params.workshopId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.workshop.id) {
      this.workshopService()
        .update(this.workshop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.workshop.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.workshopService()
        .create(this.workshop)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.workshop.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveWorkshop(workshopId): void {
    this.workshopService()
      .find(workshopId)
      .then(res => {
        this.workshop = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.variantService()
      .retrieve()
      .then(res => {
        this.variants = res.data;
      });
  }
}
