import axios from 'axios';

import { IWorkshop } from '@/shared/model/workshop.model';

const baseApiUrl = 'api/workshops';
const nameApiUrl = 'api/workshops-by-name';

export default class WorkshopService {
  public find(id: string): Promise<IWorkshop> {
    return new Promise<IWorkshop>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findByName(name: string): Promise<IWorkshop> {
    return new Promise<IWorkshop>((resolve, reject) => {
      axios
        .get(`${nameApiUrl}/${name}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IWorkshop): Promise<IWorkshop> {
    return new Promise<IWorkshop>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IWorkshop): Promise<IWorkshop> {
    return new Promise<IWorkshop>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
