import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { ICours } from '@/shared/model/cours.model';
import CoursService from './cours.service';

@Component
export default class CoursDetails extends mixins(JhiDataUtils) {
  @Inject('coursService') private coursService: () => CoursService;
  public cours: ICours = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coursId) {
        vm.retrieveCours(to.params.coursId);
      }
    });
  }

  public retrieveCours(coursId) {
    this.coursService()
      .find(coursId)
      .then(res => {
        this.cours = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
