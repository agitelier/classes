import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import GroupService from '../group/group.service';
import { IGroup } from '@/shared/model/group.model';

import VariantService from '../variant/variant.service';
import { IVariant } from '@/shared/model/variant.model';

import AlertService from '@/shared/alert/alert.service';
import { ICours, Cours } from '@/shared/model/cours.model';
import CoursService from './cours.service';

const validations: any = {
  cours: {
    symbol: {
      required,
      maxLength: maxLength(10),
    },
    name: {
      required,
      maxLength: maxLength(70),
    },
    description: {},
  },
};

@Component({
  validations,
})
export default class CoursUpdate extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('coursService') private coursService: () => CoursService;
  public cours: ICours = new Cours();

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];

  @Inject('variantService') private variantService: () => VariantService;

  public variants: IVariant[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.coursId) {
        vm.retrieveCours(to.params.coursId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.cours.variants = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.cours.id) {
      this.coursService()
        .update(this.cours)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.cours.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.coursService()
        .create(this.cours)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.cours.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveCours(coursId): void {
    this.coursService()
      .find(coursId)
      .then(res => {
        this.cours = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
    this.variantService()
      .retrieve()
      .then(res => {
        this.variants = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
