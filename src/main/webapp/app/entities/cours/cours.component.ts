import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ICours } from '@/shared/model/cours.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import JhiDataUtils from '@/shared/data/data-utils.service';

import CoursService from './cours.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Cours extends mixins(JhiDataUtils, AlertMixin) {
  @Inject('coursService') private coursService: () => CoursService;
  private removeId: string = null;

  public cours: ICours[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllCourss();
  }

  public clear(): void {
    this.retrieveAllCourss();
  }

  public retrieveAllCourss(): void {
    this.isFetching = true;

    this.coursService()
      .retrieve()
      .then(
        res => {
          this.cours = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: ICours): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeCours(): void {
    this.coursService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.cours.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllCourss();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
