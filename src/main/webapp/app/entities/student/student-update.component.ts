import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import GroupService from '../group/group.service';
import { IGroup } from '@/shared/model/group.model';

import SubGroupService from '../sub-group/sub-group.service';
import { ISubGroup } from '@/shared/model/sub-group.model';

import AlertService from '@/shared/alert/alert.service';
import { IStudent, Student } from '@/shared/model/student.model';
import StudentService from './student.service';

const validations: any = {
  student: {
    email: {
      required,
      maxLength: maxLength(70),
    },
  },
};

@Component({
  validations,
})
export default class StudentUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('studentService') private studentService: () => StudentService;
  public student: IStudent = new Student();

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];

  @Inject('subGroupService') private subGroupService: () => SubGroupService;

  public subGroups: ISubGroup[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.studentId) {
        vm.retrieveStudent(to.params.studentId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.student.id) {
      this.studentService()
        .update(this.student)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.student.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.studentService()
        .create(this.student)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.student.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveStudent(studentId): void {
    this.studentService()
      .find(studentId)
      .then(res => {
        this.student = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
    this.subGroupService()
      .retrieve()
      .then(res => {
        this.subGroups = res.data;
      });
  }
}
