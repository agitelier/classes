import axios from 'axios';

import { ISubGroup } from '@/shared/model/sub-group.model';

const baseApiUrl = 'api/sub-groups';
const groupApiUrl = 'api/subgroups-by-student-and-group';

export default class SubGroupService {
  public find(id: string): Promise<ISubGroup> {
    return new Promise<ISubGroup>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findForStudentAndGroup(studentId: string, groupId: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(`${groupApiUrl}/studentID=${studentId}&coursID=${groupId}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: ISubGroup): Promise<ISubGroup> {
    return new Promise<ISubGroup>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: ISubGroup): Promise<ISubGroup> {
    return new Promise<ISubGroup>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
