import { Component, Vue, Inject } from 'vue-property-decorator';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import StudentService from '../student/student.service';
import { IStudent } from '@/shared/model/student.model';

import GroupService from '../group/group.service';
import { IGroup } from '@/shared/model/group.model';

import AlertService from '@/shared/alert/alert.service';
import { ISubGroup, SubGroup } from '@/shared/model/sub-group.model';
import SubGroupService from './sub-group.service';

const validations: any = {
  subGroup: {
    name: {
      required,
      maxLength: maxLength(30),
    },
    group: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class SubGroupUpdate extends Vue {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('subGroupService') private subGroupService: () => SubGroupService;
  public subGroup: ISubGroup = new SubGroup();

  @Inject('studentService') private studentService: () => StudentService;

  public students: IStudent[] = [];

  @Inject('groupService') private groupService: () => GroupService;

  public groups: IGroup[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.subGroupId) {
        vm.retrieveSubGroup(to.params.subGroupId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
    this.subGroup.students = [];
  }

  public save(): void {
    this.isSaving = true;
    if (this.subGroup.id) {
      this.subGroupService()
        .update(this.subGroup)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.subGroup.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.subGroupService()
        .create(this.subGroup)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.subGroup.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveSubGroup(subGroupId): void {
    this.subGroupService()
      .find(subGroupId)
      .then(res => {
        this.subGroup = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.studentService()
      .retrieve()
      .then(res => {
        this.students = res.data;
      });
    this.groupService()
      .retrieve()
      .then(res => {
        this.groups = res.data;
      });
  }

  public getSelected(selectedVals, option): any {
    if (selectedVals) {
      for (let i = 0; i < selectedVals.length; i++) {
        if (option.id === selectedVals[i].id) {
          return selectedVals[i];
        }
      }
    }
    return option;
  }
}
