import { Component, Vue, Inject } from 'vue-property-decorator';

import { ISubGroup } from '@/shared/model/sub-group.model';
import SubGroupService from './sub-group.service';

@Component
export default class SubGroupDetails extends Vue {
  @Inject('subGroupService') private subGroupService: () => SubGroupService;
  public subGroup: ISubGroup = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.subGroupId) {
        vm.retrieveSubGroup(to.params.subGroupId);
      }
    });
  }

  public retrieveSubGroup(subGroupId) {
    this.subGroupService()
      .find(subGroupId)
      .then(res => {
        this.subGroup = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
