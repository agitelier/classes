import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { ISubGroup } from '@/shared/model/sub-group.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import SubGroupService from './sub-group.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class SubGroup extends mixins(AlertMixin) {
  @Inject('subGroupService') private subGroupService: () => SubGroupService;
  private removeId: string = null;

  public subGroups: ISubGroup[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllSubGroups();
  }

  public clear(): void {
    this.retrieveAllSubGroups();
  }

  public retrieveAllSubGroups(): void {
    this.isFetching = true;

    this.subGroupService()
      .retrieve()
      .then(
        res => {
          this.subGroups = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: ISubGroup): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeSubGroup(): void {
    this.subGroupService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.subGroup.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllSubGroups();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
