import axios from 'axios';

import { IVariant } from '@/shared/model/variant.model';

const baseApiUrl = 'api/variants';
const urlCalcforms = '/gateway/calcforms/calcforms/api/variantes';
const urlValuestream = '/gateway/valuestream/valuestream/api/variantes';

export default class VariantService {
  public findCalcformsVariantById(id: string): Promise<IVariant> {
    return new Promise<IVariant>((resolve, reject) => {
      axios
        .get(`${urlCalcforms}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieveVariantsCalcforms(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(urlCalcforms)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public findValuestreamVariantById(id: string): Promise<IVariant> {
    return new Promise<IVariant>((resolve, reject) => {
      axios
        .get(`${urlValuestream}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieveVariantsValueStream(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(urlValuestream)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public find(id: string): Promise<IVariant> {
    return new Promise<IVariant>((resolve, reject) => {
      axios
        .get(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public retrieve(): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .get(baseApiUrl)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public delete(id: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios
        .delete(`${baseApiUrl}/${id}`)
        .then(res => {
          resolve(res);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public create(entity: IVariant): Promise<IVariant> {
    return new Promise<IVariant>((resolve, reject) => {
      axios
        .post(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  public update(entity: IVariant): Promise<IVariant> {
    return new Promise<IVariant>((resolve, reject) => {
      axios
        .put(`${baseApiUrl}`, entity)
        .then(res => {
          resolve(res.data);
        })
        .catch(err => {
          reject(err);
        });
    });
  }
}
