import { mixins } from 'vue-class-component';

import { Component, Vue, Inject } from 'vue-property-decorator';
import Vue2Filters from 'vue2-filters';
import { IVariant } from '@/shared/model/variant.model';
import AlertMixin from '@/shared/alert/alert.mixin';

import JhiDataUtils from '@/shared/data/data-utils.service';

import VariantService from './variant.service';

@Component({
  mixins: [Vue2Filters.mixin],
})
export default class Variant extends mixins(JhiDataUtils, AlertMixin) {
  @Inject('variantService') private variantService: () => VariantService;
  private removeId: string = null;

  public variants: IVariant[] = [];

  public isFetching = false;

  public mounted(): void {
    this.retrieveAllVariants();
  }

  public clear(): void {
    this.retrieveAllVariants();
  }

  public retrieveAllVariants(): void {
    this.isFetching = true;

    this.variantService()
      .retrieve()
      .then(
        res => {
          this.variants = res.data;
          this.isFetching = false;
        },
        err => {
          this.isFetching = false;
        }
      );
  }

  public prepareRemove(instance: IVariant): void {
    this.removeId = instance.id;
    if (<any>this.$refs.removeEntity) {
      (<any>this.$refs.removeEntity).show();
    }
  }

  public removeVariant(): void {
    this.variantService()
      .delete(this.removeId)
      .then(() => {
        const message = this.$t('coursgroupesApp.variant.deleted', { param: this.removeId });
        this.alertService().showAlert(message, 'danger');
        this.getAlertFromStore();
        this.removeId = null;
        this.retrieveAllVariants();
        this.closeDialog();
      });
  }

  public closeDialog(): void {
    (<any>this.$refs.removeEntity).hide();
  }
}
