import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { numeric, required, minLength, maxLength, minValue, maxValue } from 'vuelidate/lib/validators';

import WorkshopService from '../workshop/workshop.service';
import { IWorkshop } from '@/shared/model/workshop.model';

import CoursService from '../cours/cours.service';
import { ICours } from '@/shared/model/cours.model';

import AlertService from '@/shared/alert/alert.service';
import { IVariant, Variant } from '@/shared/model/variant.model';
import VariantService from './variant.service';

const validations: any = {
  variant: {
    name: {
      required,
      maxLength: maxLength(50),
    },
    description: {
      required,
    },
    workshop: {
      required,
    },
  },
};

@Component({
  validations,
})
export default class VariantUpdate extends mixins(JhiDataUtils) {
  @Inject('alertService') private alertService: () => AlertService;
  @Inject('variantService') private variantService: () => VariantService;
  public variant: IVariant = new Variant();

  @Inject('workshopService') private workshopService: () => WorkshopService;

  public workshops: IWorkshop[] = [];

  @Inject('coursService') private coursService: () => CoursService;

  public cours: ICours[] = [];
  public isSaving = false;
  public currentLanguage = '';

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.variantId) {
        vm.retrieveVariant(to.params.variantId);
      }
      vm.initRelationships();
    });
  }

  created(): void {
    this.currentLanguage = this.$store.getters.currentLanguage;
    this.$store.watch(
      () => this.$store.getters.currentLanguage,
      () => {
        this.currentLanguage = this.$store.getters.currentLanguage;
      }
    );
  }

  public save(): void {
    this.isSaving = true;
    if (this.variant.id) {
      this.variantService()
        .update(this.variant)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.variant.updated', { param: param.id });
          this.alertService().showAlert(message, 'info');
        });
    } else {
      this.variantService()
        .create(this.variant)
        .then(param => {
          this.isSaving = false;
          this.$router.go(-1);
          const message = this.$t('coursgroupesApp.variant.created', { param: param.id });
          this.alertService().showAlert(message, 'success');
        });
    }
  }

  public retrieveVariant(variantId): void {
    this.variantService()
      .find(variantId)
      .then(res => {
        this.variant = res;
      });
  }

  public previousState(): void {
    this.$router.go(-1);
  }

  public initRelationships(): void {
    this.workshopService()
      .retrieve()
      .then(res => {
        this.workshops = res.data;
      });
    this.coursService()
      .retrieve()
      .then(res => {
        this.cours = res.data;
      });
  }
}
