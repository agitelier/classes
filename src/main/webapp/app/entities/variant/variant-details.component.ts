import { Component, Inject } from 'vue-property-decorator';

import { mixins } from 'vue-class-component';
import JhiDataUtils from '@/shared/data/data-utils.service';

import { IVariant } from '@/shared/model/variant.model';
import VariantService from './variant.service';

@Component
export default class VariantDetails extends mixins(JhiDataUtils) {
  @Inject('variantService') private variantService: () => VariantService;
  public variant: IVariant = {};

  beforeRouteEnter(to, from, next) {
    next(vm => {
      if (to.params.variantId) {
        vm.retrieveVariant(to.params.variantId);
      }
    });
  }

  public retrieveVariant(variantId) {
    this.variantService()
      .find(variantId)
      .then(res => {
        this.variant = res;
      });
  }

  public previousState() {
    this.$router.go(-1);
  }
}
