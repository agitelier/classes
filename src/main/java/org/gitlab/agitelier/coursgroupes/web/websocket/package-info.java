/**
 * WebSocket services, using Spring Websocket.
 */
package org.gitlab.agitelier.coursgroupes.web.websocket;
