package org.gitlab.agitelier.coursgroupes.web.rest;


import org.gitlab.agitelier.coursgroupes.domain.Group;
import org.gitlab.agitelier.coursgroupes.repository.GroupRepository;
import org.gitlab.agitelier.coursgroupes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.gitlab.agitelier.coursgroupes.domain.Group}.
 */
@RestController
@RequestMapping("/api")
public class GroupResource {

    private final Logger log = LoggerFactory.getLogger(GroupResource.class);

    private static final String ENTITY_NAME = "group";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GroupRepository groupRepository;

    public GroupResource(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    /**
     * {@code POST  /groups} : Create a new group.
     *
     * @param group the group to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new group, or with status {@code 400 (Bad Request)} if the group has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/groups")
    public ResponseEntity<Group> createGroup(@Valid @RequestBody Group group) throws URISyntaxException {
        log.debug("REST request to save Group : {}", group);
        if (group.getId() != null) {
            throw new BadRequestAlertException("A new group cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (groupExist(group.getNumber(), group.getCours().getId(), group.getId())) {
            throw new BadRequestAlertException("A new group cannot have the same group number for the same course", ENTITY_NAME, "numbercousesexists");
        }
        Group result = groupRepository.save(group);
        return ResponseEntity.created(new URI("/api/groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /groups} : Updates an existing group.
     *
     * @param group the group to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated group,
     * or with status {@code 400 (Bad Request)} if the group is not valid,
     * or with status {@code 500 (Internal Server Error)} if the group couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/groups")
    public ResponseEntity<Group> updateGroup(@Valid @RequestBody Group group) throws URISyntaxException {
        log.debug("REST request to update Group : {}", group);
        if (group.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (groupExist(group.getNumber(), group.getCours().getId(), group.getId())) {
            throw new BadRequestAlertException("A new group cannot have the same group number for the same course", ENTITY_NAME, "numbercousesexists");
        }
        Group result = groupRepository.save(group);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, group.getId()))
            .body(result);
    }

    /**
     * {@code GET  /groups} : get all the groups.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of groups in body.
     */
    @GetMapping("/groups")
    public List<Group> getAllGroups(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all Groups");
        return groupRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /groups/:id} : get the "id" group.
     *
     * @param id the id of the group to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the group, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/groups/{id}")
    public ResponseEntity<Group> getGroup(@PathVariable String id) {
        log.debug("REST request to get Group : {}", id);
        Optional<Group> group = groupRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(group);
    }

    /**
     * {@code GET /groups} : get all groups by the "teacherID".
     *
     * @param teacherID the teacherID of the group to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the group, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/groups-by-teacher/{teacherID}")
    public List<Group> getGroupByTeacherID(@PathVariable String teacherID) {
        log.debug("REST request to get Group for: {}", teacherID);
        return groupRepository.findAllByTeacherID(teacherID);
    }

    /**
     * {@code GET /groups} : get all groups by the "teacherID".
     *
     * @param teacherID the teacherID of the group to retrieve.
     * @param coursId the coursId of the group to retrieve
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the group, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/groups-by-teacher-and-cours/teacherID={teacherID}&coursId={coursId}")
    public List<Group> getGroupByTeacherIDAndCoursId(@PathVariable String teacherID, @PathVariable String coursId) {
        log.debug("REST request to get Group for: {} and {}", teacherID, coursId);
        return groupRepository.findAllByTeacherIDAndIdCours(teacherID, coursId);
    }

    /**
     * {@code GET /groups} : get all groups by studentId
     *
     * @param studentId the student of the group to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the group, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/groups-by-student/{studentId}")
    public List<Group> getGroupByStudent(@PathVariable String studentId) {
        log.debug("REST request to get Group for: {}", studentId);
        return groupRepository.findAllByIdStudents(studentId);
    }

    /**
     * {@code GET /groups} : get the groups by coursId.
     *
     * @param coursId the cours of the group to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the group, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/groups-by-cours/{coursId}")
    public List<Group> getGroupByCours(@PathVariable String coursId) {
        log.debug("REST request to get Group for: {}", coursId);
        return groupRepository.findAllByIdCours(coursId);
    }

    /**
     * {@code GET /groups} : get the groups by number and coursId.
     *
     * @param number the group number to look for.
     * @param coursId the coursId to look for.
     * @param groupId the id of the group we are updatin or creating
     * @return true if the group is found.
     */
    @GetMapping("/group-by-cours-number/number={number}&coursId={coursId}")
    public boolean groupExist(@PathVariable Integer number, @PathVariable String coursId, String groupId) {
        log.debug("REST request to validate for: {} and {}", number, coursId);
        boolean groupExist = false;
        if (groupRepository.findGroupByNumberAndIdCours(number, coursId) != null && !groupRepository.findGroupByNumberAndIdCours(number, coursId).getId().equals(groupId)) {
            groupExist = true;
        }
        return groupExist;
    }

    /**
     * {@code DELETE  /groups/:id} : delete the "id" group.
     *
     * @param id the id of the group to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/groups/{id}")
    public ResponseEntity<Void> deleteGroup(@PathVariable String id) {
        log.debug("REST request to delete Group : {}", id);
        groupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
