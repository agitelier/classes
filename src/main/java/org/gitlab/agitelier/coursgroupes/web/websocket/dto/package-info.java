/**
 * Data Access Objects used by WebSocket services.
 */
package org.gitlab.agitelier.coursgroupes.web.websocket.dto;
