package org.gitlab.agitelier.coursgroupes.web.rest;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.gitlab.agitelier.coursgroupes.domain.Student;
import org.gitlab.agitelier.coursgroupes.domain.SubGroup;
import org.gitlab.agitelier.coursgroupes.repository.SubGroupRepository;
import org.gitlab.agitelier.coursgroupes.web.rest.errors.BadRequestAlertException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.gitlab.agitelier.coursgroupes.domain.SubGroup}.
 */
@RestController
@RequestMapping("/api")
public class SubGroupResource {

    private static final String ENTITY_NAME = "subGroup";
    private final Logger log = LoggerFactory.getLogger(SubGroupResource.class);
    private final SubGroupRepository subGroupRepository;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;


/*
    public boolean IsStudentPartOfGroup(String idGroup, String idStudent)
    {
        return (subGroupRepository.findByIdGroupAndIdStudent(idGroup, idStudent).size() > 0);

    }
*/

    public SubGroupResource(SubGroupRepository subGroupRepository) {
        this.subGroupRepository = subGroupRepository;
    }

    public boolean IsStudentsAlreadyInGroup(SubGroup subGroup) {
        String idGroup = subGroup.getGroup().getId();
        String idSubGroup = subGroup.getId();
        java.util.Iterator<Student> iter = subGroup.getStudents().iterator();

        while (iter.hasNext()) {
            String idStudent = iter.next().getId();

            java.util.Iterator<SubGroup> iterSubGroupWithStudent = subGroupRepository.findByIdGroupAndIdStudent(idGroup, idStudent).iterator();

            while (iterSubGroupWithStudent.hasNext()) {
                String idSubGroupWithStudent = iterSubGroupWithStudent.next().getId();

                if (!idSubGroupWithStudent.equals(idSubGroup)) {
                    //throw new BadRequestAlertException("A student is already in same group " + idSubGroupWithStudent + " // " + idSubGroup, ENTITY_NAME, "studentalreadyingroup");
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * {@code POST  /sub-groups} : Create a new subGroup.
     *
     * @param subGroup the subGroup to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new subGroup, or with status {@code 400 (Bad Request)} if the subGroup has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/sub-groups")
    public ResponseEntity<SubGroup> createSubGroup(@Valid @RequestBody SubGroup subGroup) throws URISyntaxException {
        log.debug("REST request to save SubGroup : {}", subGroup);
        if (subGroup.getId() != null) {
            throw new BadRequestAlertException("A new subGroup cannot already have an ID", ENTITY_NAME, "idexists");
        }

        if (IsStudentsAlreadyInGroup(subGroup)) {
            throw new BadRequestAlertException("A student is already in same group ", ENTITY_NAME, "studentalreadyingroup");
        }


        SubGroup result = subGroupRepository.save(subGroup);


        return ResponseEntity.created(new URI("/api/sub-groups/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /sub-groups} : Updates an existing subGroup.
     *
     * @param subGroup the subGroup to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated subGroup,
     * or with status {@code 400 (Bad Request)} if the subGroup is not valid,
     * or with status {@code 500 (Internal Server Error)} if the subGroup couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/sub-groups")
    public ResponseEntity<SubGroup> updateSubGroup(@Valid @RequestBody SubGroup subGroup) throws URISyntaxException {
        log.debug("REST request to update SubGroup : {}", subGroup);
        if (subGroup.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }

        if (IsStudentsAlreadyInGroup(subGroup)) {
            throw new BadRequestAlertException("A student is already in same group ", ENTITY_NAME, "studentalreadyingroup");
        }


        SubGroup result = subGroupRepository.save(subGroup);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, subGroup.getId()))
            .body(result);
    }

    /**
     * {@code GET  /sub-groups} : get all the subGroups by group.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subGroups in body.
     */
    @GetMapping("/sub-groups-by-group/{idGroup}")
    public List<SubGroup> getSubGroupsByGroup(@PathVariable String idGroup) {
        log.debug("REST request to get SubGroups by Group");
        //Group group = new Group();
        //group.setId(idGroup);
        return subGroupRepository.findByIdGroup(idGroup);
    }


    /**
     * {@code GET  /sub-groups} : get all the subGroups by name.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subGroups in body.
     */
    @GetMapping("/sub-groups-by-group-end-student/idGroup={idGroup}&idStudent={idStudent}")
    public List<SubGroup> getSubGroupsByGroupAndStudent(@PathVariable String idGroup, @PathVariable String idStudent) {
        log.debug("REST request to get SubGroups by Name");
        return subGroupRepository.findByIdGroupAndIdStudent(idGroup, idStudent);
    }


    /**
     * {@code GET  /sub-groups} : get all the subGroups by name.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subGroups in body.
     */
    @GetMapping("/sub-groups-by-name/{name}")
    public List<SubGroup> getSubGroupsByName(@PathVariable String name) {
        log.debug("REST request to get SubGroups by Name");
        return subGroupRepository.findByName(name);
    }

    /**
     * {@code GET  /sub-groups} : get all the subGroups by student.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subGroups in body.
     */
    @GetMapping("/sub-groups-by-student/{idStudent}")
    public List<SubGroup> getSubGroupsByStudent(@PathVariable String idStudent) {
        log.debug("REST request to get SubGroups by Student with ID " + idStudent);
        //Student student = new Student();
        //student.setId(idStudent);
        return subGroupRepository.findByIdStudent(idStudent);
    }

    /**
     * {@code GET  /subgroups-by-student-and-group} : get the subGroups by student and group.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the subGroup in body.
     */
    @GetMapping("/subgroups-by-student-and-group/studentID={studentId}&coursID={groupId}")
    public SubGroup getSubGroupsByStudentAndGroup(@PathVariable String studentId,
                                                  @PathVariable String groupId) {
        log.debug("REST request to get SubGroups by Student" +
            studentId + " and Group " + groupId);

        return subGroupRepository.findByIdStudentAndGroup(studentId, groupId);
    }

    /**
     * {@code GET  /sub-groups} : get all the subGroups.
     *
     * @param eagerload flag to eager load entities from relationships (This is applicable for many-to-many).
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of subGroups in body.
     */
    @GetMapping("/sub-groups")
    public List<SubGroup> getAllSubGroups(@RequestParam(required = false, defaultValue = "false") boolean eagerload) {
        log.debug("REST request to get all SubGroups");
        return subGroupRepository.findAllWithEagerRelationships();
    }

    /**
     * {@code GET  /sub-groups/:id} : get the "id" subGroup.
     *
     * @param id the id of the subGroup to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the subGroup, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/sub-groups/{id}")
    public ResponseEntity<SubGroup> getSubGroup(@PathVariable String id) {
        log.debug("REST request to get SubGroup : {}", id);
        Optional<SubGroup> subGroup = subGroupRepository.findOneWithEagerRelationships(id);
        return ResponseUtil.wrapOrNotFound(subGroup);
    }

    /**
     * {@code DELETE  /sub-groups/:id} : delete the "id" subGroup.
     *
     * @param id the id of the subGroup to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/sub-groups/{id}")
    public ResponseEntity<Void> deleteSubGroup(@PathVariable String id) {
        log.debug("REST request to delete SubGroup : {}", id);
        subGroupRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
