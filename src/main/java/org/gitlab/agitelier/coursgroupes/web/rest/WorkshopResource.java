package org.gitlab.agitelier.coursgroupes.web.rest;

import org.gitlab.agitelier.coursgroupes.domain.Workshop;
import org.gitlab.agitelier.coursgroupes.repository.WorkshopRepository;
import org.gitlab.agitelier.coursgroupes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.gitlab.agitelier.coursgroupes.domain.Workshop}.
 */
@RestController
@RequestMapping("/api")
public class WorkshopResource {

    private final Logger log = LoggerFactory.getLogger(WorkshopResource.class);

    private static final String ENTITY_NAME = "workshop";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final WorkshopRepository workshopRepository;

    public WorkshopResource(WorkshopRepository workshopRepository) {
        this.workshopRepository = workshopRepository;
    }

    /**
     * {@code POST  /workshops} : Create a new workshop.
     *
     * @param workshop the workshop to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new workshop, or with status {@code 400 (Bad Request)} if the workshop has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/workshops")
    public ResponseEntity<Workshop> createWorkshop(@Valid @RequestBody Workshop workshop) throws URISyntaxException {
        log.debug("REST request to save Workshop : {}", workshop);
        if (workshop.getId() != null) {
            throw new BadRequestAlertException("A new workshop cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Workshop result = workshopRepository.save(workshop);
        return ResponseEntity.created(new URI("/api/workshops/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /workshops} : Updates an existing workshop.
     *
     * @param workshop the workshop to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated workshop,
     * or with status {@code 400 (Bad Request)} if the workshop is not valid,
     * or with status {@code 500 (Internal Server Error)} if the workshop couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/workshops")
    public ResponseEntity<Workshop> updateWorkshop(@Valid @RequestBody Workshop workshop) throws URISyntaxException {
        log.debug("REST request to update Workshop : {}", workshop);
        if (workshop.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Workshop result = workshopRepository.save(workshop);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, workshop.getId()))
            .body(result);
    }

    /**
     * {@code GET  /workshops} : get all the workshops.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of workshops in body.
     */
    @GetMapping("/workshops")
    public List<Workshop> getAllWorkshops() {
        log.debug("REST request to get all Workshops");
        return workshopRepository.findAll();
    }

    /**
     * {@code GET  /workshops/:id} : get the "id" workshop.
     *
     * @param id the id of the workshop to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workshop, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/workshops/{id}")
    public ResponseEntity<Workshop> getWorkshop(@PathVariable String id) {
        log.debug("REST request to get Workshop : {}", id);
        Optional<Workshop> workshop = workshopRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(workshop);
    }

    /**
     * {@code GET  /workshops-by-name/:name} : get the "name" workshop.
     *
     * @param name the name of the workshop to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the workshop, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/workshops-by-name/{name}")
    public ResponseEntity<Workshop> getWorkshopbyName(@PathVariable String name) {
        log.debug("REST request to get Workshop : {}", name);
        Optional<Workshop> workshop = workshopRepository.findOneWithEagerRelationships(name);
        return ResponseUtil.wrapOrNotFound(workshop);
    }

    /**
     * {@code DELETE  /workshops/:id} : delete the "id" workshop.
     *
     * @param id the id of the workshop to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/workshops/{id}")
    public ResponseEntity<Void> deleteWorkshop(@PathVariable String id) {
        log.debug("REST request to delete Workshop : {}", id);
        workshopRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }
}
