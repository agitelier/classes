/**
 * View Models used by Spring MVC REST controllers.
 */
package org.gitlab.agitelier.coursgroupes.web.rest.vm;
