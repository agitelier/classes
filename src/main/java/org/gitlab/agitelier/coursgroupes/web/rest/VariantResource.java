package org.gitlab.agitelier.coursgroupes.web.rest;

import org.gitlab.agitelier.coursgroupes.domain.Variant;
import org.gitlab.agitelier.coursgroupes.repository.VariantRepository;
import org.gitlab.agitelier.coursgroupes.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link org.gitlab.agitelier.coursgroupes.domain.Variant}.
 */
@RestController
@RequestMapping("/api")
public class VariantResource {

    private final Logger log = LoggerFactory.getLogger(VariantResource.class);

    private static final String ENTITY_NAME = "variant";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final VariantRepository variantRepository;

    public VariantResource(VariantRepository variantRepository) {
        this.variantRepository = variantRepository;
    }

    /**
     * {@code POST  /variants} : Create a new variant.
     *
     * @param variant the variant to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new variant, or with status {@code 400 (Bad Request)} if the variant has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/variants")
    public ResponseEntity<Variant> createVariant(@Valid @RequestBody Variant variant) throws URISyntaxException {
        log.debug("REST request to save Variant : {}", variant);
        if (variant.getId() != null) {
            throw new BadRequestAlertException("A new variant cannot already have an ID", ENTITY_NAME, "idexists");
        }
        if (variantExist(variant.getName(), variant.getWorkshop().getId())) {
            throw new BadRequestAlertException("Variant already exists", ENTITY_NAME, "nameAndWorkshopexists");
        }
        Variant result = variantRepository.save(variant);
        return ResponseEntity.created(new URI("/api/variants/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId()))
            .body(result);
    }

    /**
     * {@code PUT  /variants} : Updates an existing variant.
     *
     * @param variant the variant to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated variant,
     * or with status {@code 400 (Bad Request)} if the variant is not valid,
     * or with status {@code 500 (Internal Server Error)} if the variant couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/variants")
    public ResponseEntity<Variant> updateVariant(@Valid @RequestBody Variant variant) throws URISyntaxException {
        log.debug("REST request to update Variant : {}", variant);
        if (variant.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Variant result = variantRepository.save(variant);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, variant.getId()))
            .body(result);
    }

    /**
     * {@code GET  /variants} : get all the variants.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of variants in body.
     */
    @GetMapping("/variants")
    public List<Variant> getAllVariants() {
        log.debug("REST request to get all Variants");
        return variantRepository.findAll();
    }

    /**
     * {@code GET  /variants/:id} : get the "id" variant.
     *
     * @param id the id of the variant to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the variant, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/variants/{id}")
    public ResponseEntity<Variant> getVariant(@PathVariable String id) {
        log.debug("REST request to get Variant : {}", id);
        Optional<Variant> variant = variantRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(variant);
    }

    /**
     * {@code DELETE  /variants/:id} : delete the "id" variant.
     *
     * @param id the id of the variant to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/variants/{id}")
    public ResponseEntity<Void> deleteVariant(@PathVariable String id) {
        log.debug("REST request to delete Variant : {}", id);
        variantRepository.deleteById(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id)).build();
    }

    /**
     * {@code GET  /variants} : get all the Variants by name.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Variants in body.
     */
    @GetMapping("/Variant-by-name/idVariant={variantName}")
    public List<Variant> getVariantByName(@PathVariable String variantName)  {
        log.debug("REST request to get Variants by Name");
        return variantRepository.findByName(variantName);
    }

    /**
     * {code GET /variants} : Find a variant by name and workshop.
     *
     * @param name the name of the variant
     * @param idWorkshop the id of the workshop
     * @return true if the variant is found.
     */
    @GetMapping("/variant-by-name-and-workshop/name={name}&idWorkshop={idWorkshop}")
    public boolean variantExist(@PathVariable String name, @PathVariable String idWorkshop) {
        log.debug("REST request to validate for: {} and {}", name, idWorkshop);
        boolean variantExist = false;
        if (variantRepository.findVariantByNameAndIdWorkshop(name, idWorkshop) != null) {
            variantExist = true;
        }
        return variantExist;
    }

    /**
     * {@code GET  /variants} : get all the Variants by cours.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of Variants in body.
     */
    @GetMapping("/Variant-by-cours/idVariant={idVariant} && idCours={idCours}")
    public List<Variant> getVariantByCours(@PathVariable String idCours)  {
        log.debug("REST request to get Variants by cours");
        return variantRepository.findByIdCours(idCours);
    }

}
