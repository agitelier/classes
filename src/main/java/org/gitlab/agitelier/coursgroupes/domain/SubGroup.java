package org.gitlab.agitelier.coursgroupes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A SubGroup.
 */
@Document(collection = "sub_group")
public class SubGroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(max = 30)
    @Field("name")
    private String name;

    @DBRef
    @Field("students")
    private Set<Student> students = new HashSet<>();

    @DBRef
    @Field("group")
    @JsonIgnoreProperties(value = "subGroups", allowSetters = true)
    private Group group;

    public SubGroup() {}

    public SubGroup(String name) {
        this.name = name;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public SubGroup name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public SubGroup students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public SubGroup addStudent(Student student) {
        this.students.add(student);
        student.getSubgroups().add(this);
        return this;
    }

    public SubGroup removeStudent(Student student) {
        this.students.remove(student);
        student.getSubgroups().remove(this);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Group getGroup() {
        return group;
    }

    public SubGroup group(Group group) {
        this.group = group;
        return this;
    }

    public void setGroup(Group group) {
        this.group = group;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof SubGroup)) {
            return false;
        }
        return id != null && id.equals(((SubGroup) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "SubGroup{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            "}";
    }
}
