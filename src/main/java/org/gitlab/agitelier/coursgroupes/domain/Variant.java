package org.gitlab.agitelier.coursgroupes.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Variant.
 */
@Document(collection = "variant")
public class Variant implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(max = 50)
    @Field("name")
    private String name;

    
    @Field("description")
    private String description;

    @DBRef
    @Field("workshop")
    @JsonIgnoreProperties(value = "variants", allowSetters = true)
    private Workshop workshop;

    @DBRef
    @Field("cours")
    @JsonIgnore
    private Set<Cours> cours = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Variant name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Variant description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Workshop getWorkshop() {
        return workshop;
    }

    public Variant workshop(Workshop workshop) {
        this.workshop = workshop;
        return this;
    }

    public void setWorkshop(Workshop workshop) {
        this.workshop = workshop;
    }

    public Set<Cours> getCours() {
        return cours;
    }

    public Variant cours(Set<Cours> cours) {
        this.cours = cours;
        return this;
    }

    public Variant addCours(Cours cours) {
        this.cours.add(cours);
        cours.getVariants().add(this);
        return this;
    }

    public Variant removeCours(Cours cours) {
        this.cours.remove(cours);
        cours.getVariants().remove(this);
        return this;
    }

    public void setCours(Set<Cours> cours) {
        this.cours = cours;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Variant)) {
            return false;
        }
        return id != null && id.equals(((Variant) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Variant{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
