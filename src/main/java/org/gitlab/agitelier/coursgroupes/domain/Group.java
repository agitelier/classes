package org.gitlab.agitelier.coursgroupes.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Group.
 */
@Document(collection = "group")
public class Group implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Min(value = 0)
    @Max(value = 99)
    @Field("number")
    private Integer number;

    @NotNull
    @Size(max = 70)
    @Field("teacher_id")
    private String teacherID;

    @Field("description")
    private String description;

    @DBRef
    @Field("subGroup")
    private Set<SubGroup> subGroups = new HashSet<>();

    @DBRef
    @Field("students")
    private Set<Student> students = new HashSet<>();

    @DBRef
    @Field("cours")
    @JsonIgnoreProperties(value = "groups", allowSetters = true)
    private Cours cours;

    public Group() {}

    public Group(int number, String teacherID, String description) {
        this.number = number;
        this.teacherID = teacherID;
        this.description = description;
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getNumber() {
        return number;
    }

    public Group number(Integer number) {
        this.number = number;
        return this;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public String getTeacherID() {
        return teacherID;
    }

    public Group teacherID(String teacherID) {
        this.teacherID = teacherID;
        return this;
    }

    public void setTeacherID(String teacherID) {
        this.teacherID = teacherID;
    }

    public String getDescription() {
        return description;
    }

    public Group description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<SubGroup> getSubGroups() {
        return subGroups;
    }

    public Group subGroups(Set<SubGroup> subGroups) {
        this.subGroups = subGroups;
        return this;
    }

    public Group addSubGroup(SubGroup subGroup) {
        this.subGroups.add(subGroup);
        subGroup.setGroup(this);
        return this;
    }

    public Group removeSubGroup(SubGroup subGroup) {
        this.subGroups.remove(subGroup);
        subGroup.setGroup(null);
        return this;
    }

    public void setSubGroups(Set<SubGroup> subGroups) {
        this.subGroups = subGroups;
    }

    public Set<Student> getStudents() {
        return students;
    }

    public Group students(Set<Student> students) {
        this.students = students;
        return this;
    }

    public Group addStudent(Student student) {
        this.students.add(student);
        student.getGroups().add(this);
        return this;
    }

    public Group removeStudent(Student student) {
        this.students.remove(student);
        student.getGroups().remove(this);
        return this;
    }

    public void setStudents(Set<Student> students) {
        this.students = students;
    }

    public Cours getCours() {
        return cours;
    }

    public Group cours(Cours cours) {
        this.cours = cours;
        return this;
    }

    public void setCours(Cours cours) {
        this.cours = cours;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Group)) {
            return false;
        }
        return id != null && id.equals(((Group) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Group{" +
            "id=" + getId() +
            ", number=" + getNumber() +
            ", teacherID='" + getTeacherID() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
