package org.gitlab.agitelier.coursgroupes.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Student.
 */
@Document(collection = "student")
public class Student implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(max = 70)
    @Field("email")
    private String email;

    @DBRef
    @Field("groups")
    @JsonIgnore
    private Set<Group> groups = new HashSet<>();

    @DBRef
    @Field("subgroups")
    @JsonIgnore
    private Set<SubGroup> subgroups = new HashSet<>();

    public Student() {}

    public Student(String email) {
        this.email(email);
    }

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public Student email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public Student groups(Set<Group> groups) {
        this.groups = groups;
        return this;
    }

    public Student addGroup(Group group) {
        this.groups.add(group);
        group.getStudents().add(this);
        return this;
    }

    public Student removeGroup(Group group) {
        this.groups.remove(group);
        group.getStudents().remove(this);
        return this;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<SubGroup> getSubgroups() {
        return subgroups;
    }

    public Student subgroups(Set<SubGroup> subGroups) {
        this.subgroups = subGroups;
        return this;
    }

    public Student addSubgroup(SubGroup subGroup) {
        this.subgroups.add(subGroup);
        subGroup.getStudents().add(this);
        return this;
    }

    public Student removeSubgroup(SubGroup subGroup) {
        this.subgroups.remove(subGroup);
        subGroup.getStudents().remove(this);
        return this;
    }

    public void setSubgroups(Set<SubGroup> subGroups) {
        this.subgroups = subGroups;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Student)) {
            return false;
        }
        return id != null && id.equals(((Student) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Student{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            "}";
    }
}
