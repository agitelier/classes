package org.gitlab.agitelier.coursgroupes.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.DBRef;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Cours.
 */
@Document(collection = "cours")
public class Cours implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    private String id;

    @NotNull
    @Size(max = 10)
    @Field("symbol")
    private String symbol;

    @NotNull
    @Size(max = 70)
    @Field("name")
    private String name;

    @Field("description")
    private String description;

    @DBRef
    @Field("group")
    private Set<Group> groups = new HashSet<>();

    @DBRef
    @Field("variants")
    private Set<Variant> variants = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSymbol() {
        return symbol;
    }

    public Cours symbol(String symbol) {
        this.symbol = symbol;
        return this;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public Cours name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public Cours description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Group> getGroups() {
        return groups;
    }

    public Cours groups(Set<Group> groups) {
        this.groups = groups;
        return this;
    }

    public Cours addGroup(Group group) {
        this.groups.add(group);
        group.setCours(this);
        return this;
    }

    public Cours removeGroup(Group group) {
        this.groups.remove(group);
        group.setCours(null);
        return this;
    }

    public void setGroups(Set<Group> groups) {
        this.groups = groups;
    }

    public Set<Variant> getVariants() {
        return variants;
    }

    public Cours variants(Set<Variant> variants) {
        this.variants = variants;
        return this;
    }

    public Cours addVariant(Variant variant) {
        this.variants.add(variant);
        variant.getCours().add(this);
        return this;
    }

    public Cours removeVariant(Variant variant) {
        this.variants.remove(variant);
        variant.getCours().remove(this);
        return this;
    }

    public void setVariants(Set<Variant> variants) {
        this.variants = variants;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Cours)) {
            return false;
        }
        return id != null && id.equals(((Cours) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Cours{" +
            "id=" + getId() +
            ", symbol='" + getSymbol() + "'" +
            ", name='" + getName() + "'" +
            ", description='" + getDescription() + "'" +
            "}";
    }
}
