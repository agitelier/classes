package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.Group;
import org.gitlab.agitelier.coursgroupes.domain.Workshop;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import  java.util.Optional;

/**
 * Spring Data MongoDB repository for the Workshop entity.
 */
@Repository
public interface WorkshopRepository extends MongoRepository<Workshop, String> {

    @Query("{'name': ?0}")
    Optional<Workshop> findOneWithEagerRelationships(String name);
}
