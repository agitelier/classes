package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.Cours;

import org.gitlab.agitelier.coursgroupes.domain.Group;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Cours entity.
 */
@Repository
public interface CoursRepository extends MongoRepository<Cours, String> {

    @Query("{}")
    Page<Cours> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Cours> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Cours> findOneWithEagerRelationships(String id);

    @Query("{'name': ?0}")
    List<Cours> findCoursByName(String name);

    @Query("{'symbol': ?0}")
    List<Cours> findCoursBySymbol(String symbol);

}
