package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.SubGroup;
import org.gitlab.agitelier.coursgroupes.domain.Group;
import org.gitlab.agitelier.coursgroupes.domain.Student;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the SubGroup entity.
 */
@Repository
public interface SubGroupRepository extends MongoRepository<SubGroup, String> {

    @Query("{}")
    Page<SubGroup> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<SubGroup> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<SubGroup> findOneWithEagerRelationships(String id);


    @Query("{ 'students.id' : ?0 }")
    List<SubGroup> findByIdStudent(String idStudent);

    @Query("{'students.id': ?0, 'group.id' : ?1}")
    SubGroup findByIdStudentAndGroup(String idStudent, String idGroup);

    @Query("{ 'name' : ?0 }")
    List<SubGroup> findByName(String name);


    @Query("{ 'group.id' : ?0 }")
    List<SubGroup> findByIdGroup(String idGroup);


    @Query("{ 'group.id' : ?0, 'students.id' : ?1 }")
    List<SubGroup> findByIdGroupAndIdStudent(String idGroup, String idStudent);


}
