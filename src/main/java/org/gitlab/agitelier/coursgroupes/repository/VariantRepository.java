package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.Variant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Variant entity.
 */
@SuppressWarnings("unused")
@Repository
public interface VariantRepository extends MongoRepository<Variant, String> {
    @Query("{}")
    Page<Variant> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Variant> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Variant> findOneWithEagerRelationships(String id);

    @Query("{'name': ?0, 'workshop.id' : ?1}")
    Variant findVariantByNameAndIdWorkshop(String name, String idWorkshop);

    @Query("{ 'variant.id : ?0 }")
    List<Variant> findByIdVariant(String idVariant);

    @Query("{ 'name' : ?0 }")
    List<Variant> findByName(String name);

    @Query("{ 'cours.id : ?0 }")
    List<Variant> findByIdCours(String idCours);

    @Query("{ 'variant.id : ?0, 'cours.id : ?1' }")
    List<Variant> findByIdVariantAndIdCours(String idVariant, String idCours);



}

