package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.Group;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

/**
 * Spring Data MongoDB repository for the Group entity.
 */
@Repository
public interface GroupRepository extends MongoRepository<Group, String> {

    @Query("{}")
    Page<Group> findAllWithEagerRelationships(Pageable pageable);

    @Query("{}")
    List<Group> findAllWithEagerRelationships();

    @Query("{'id': ?0}")
    Optional<Group> findOneWithEagerRelationships(String id);

    @Query("{'teacherID': ?0}")
    List<Group> findAllByTeacherID(String teacherId);

    @Query("{'student.id': ?0}")
    List<Group> findAllByIdStudents(String studentId);

    @Query("{'cours.id': ?0}")

    List<Group> findAllByIdCours(String coursId);

    @Query("{'teacherID': ?0, 'cours.id': ?1}")
    List<Group> findAllByTeacherIDAndIdCours(String teacherId, String IdCours);

    @Query("{'number': ?0, 'cours.id' : ?1}")
    Group findGroupByNumberAndIdCours(Integer Number, String IdCours );


}
