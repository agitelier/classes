package org.gitlab.agitelier.coursgroupes.repository;

import org.gitlab.agitelier.coursgroupes.domain.Authority;

import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Spring Data MongoDB repository for the {@link Authority} entity.
 */
public interface AuthorityRepository extends MongoRepository<Authority, String> {
}
