package org.gitlab.agitelier.coursgroupes.config.dbmigrations;

import org.gitlab.agitelier.coursgroupes.domain.*;
import org.gitlab.agitelier.coursgroupes.security.AuthoritiesConstants;

import com.github.mongobee.changeset.ChangeLog;
import com.github.mongobee.changeset.ChangeSet;
import org.springframework.context.annotation.Profile;
import org.springframework.data.mongodb.core.MongoTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Creates the initial database setup.
 */
@ChangeLog(order = "001")
public class InitialSetupMigration {

    @ChangeSet(order = "01", author = "initiator", id = "01-addAuthorities")
    public void addAuthorities(MongoTemplate mongoTemplate) {
        Authority adminAuthority = new Authority();
        adminAuthority.setName(AuthoritiesConstants.ADMIN);
        Authority userAuthority = new Authority();
        userAuthority.setName(AuthoritiesConstants.USER);
        mongoTemplate.save(adminAuthority);
        mongoTemplate.save(userAuthority);
    }

    private final Random ran = new Random();

    private final String[] students = {
        "rhumeteau@hotmail.com",
        "douceur@gmail.ca",
        "impossibilit@intriguant.qc.ca",
        "monsieurlavoie@ens.uqam.ca",
        "etudiant_intense02@hotmail.com",
        "beautefatale@gmail.ca",
        "changement_de_loisir@intriguant.qc.ca",
        "piece_videL0L@ens.uqam.ca",
        "user"
    };

    private final String[] teachers = {
        "admin"
    };

    private final String[] subgroups = {
        "Équipe de feu",
        "Équipe de glace",
        "Équipe d'acide",
        "Équipe de barbe a papa"
    };

    private final String[] coursName = {
        "Engineering",
        "Chimie Organique III",
        "Agilité pour les moins agiles",
        "Sensibilité pour les non-poètes",
        "Gradation sur l'échelle de Richeter",
        "Histoire des civilisations (mais celles qui sont capout)"
    };

    private final String[] wsName = {
        "Quizz sur les écureuils",
        "Formulaire de calcul",
        "Piscine hors-terre",
        "Choix des cylindres a moteur",
        "Troublante vérité",
        "Jeux d'amour",
        "Jeux pestilentiels",
        "Crayons colorés"
    };

    private final List<Student> sList = new ArrayList<>();

    @Profile("dev")
    @ChangeSet(order = "02", author = "Yannick", id = "student")
    public void addDevStudent(MongoTemplate mongoTemplate) {
        for (String email : students) {
            Student student = new Student();
            student.setEmail(email);
            mongoTemplate.save(student);
            sList.add(student);
        }
    }

    private final List<Workshop> wsList = new ArrayList<>();

    @Profile("dev")
    @ChangeSet(order = "03", author = "Yannick", id = "workshop")
    public void addDevWorkshop(MongoTemplate mongoTemplate) {
        for (String name : wsName) {
            Workshop ws = new Workshop();
            ws.name(name).url("www.agitelier.com/api/'"+ws.getName()+"'");
            mongoTemplate.save(ws);
            wsList.add(ws);
        }
    }

    private final List<Variant> vList = new ArrayList<>();

    @Profile("dev")
    @ChangeSet(order = "04", author = "Yannick", id = "variant")
    public void addDevVariant(MongoTemplate mongoTemplate) {
        for (Workshop ws : wsList) {
            for (int i = 0; i < 3; i++) {
                Variant variant = new Variant();
                variant.name(ws.getName() + "(V" + (i+1) + ")").workshop(ws).description("Une variante excitante.");
                mongoTemplate.save(variant);
                vList.add(variant);
            }
        }
    }

    public void addStudentsToGroupAndSubGroup(Group group, SubGroup sub) {
        Student s1 = sList.get(ran.nextInt(sList.size()));
        Student s2 = sList.get(ran.nextInt(sList.size()));
        group.addStudent(s1);
        group.addStudent(s2);
        sub.addStudent(s1);
        sub.addStudent(s2);
    }

    public void addSubGroupsToGroup(Group group, MongoTemplate mongoTemplate) {
            for (int j = 0; j < 2; j++) {
                SubGroup subgroup = new SubGroup(subgroups[j]);
                group.addSubGroup(subgroup);
                addStudentsToGroupAndSubGroup(group, subgroup);
                mongoTemplate.save(subgroup);
            }
    }

    private void addGroupsToCours(Cours cours, MongoTemplate mongoTemplate) {
        for (int i = 1; i < 4; i++) {
            Group group = new Group(i+i*10, teachers[0], "Le cours se donnera très très tôt le matin, bande de lâches.");
            cours.addGroup(group);
            mongoTemplate.save(group);
            addSubGroupsToGroup(group, mongoTemplate);
            mongoTemplate.save(group);
        }
    }

    @Profile("dev")
    @ChangeSet(order = "06", author = "Yannick", id = "cours")
    public void addDevCours(MongoTemplate mongoTemplate) {
        for (int i = 0; i < coursName.length ; i++) {
            Cours cours = new Cours();
            cours.name(coursName[i]).description("Cours minable avec certaines qualités agréables").symbol("INF210"+i);
            mongoTemplate.save(cours);
            addGroupsToCours(cours, mongoTemplate);
            for (int j = 0; j < 3 ; j++) {
                cours.addVariant(vList.get(ran.nextInt(vList.size())));
            }
            mongoTemplate.save(cours);
        }
    }
}
