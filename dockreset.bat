CALL docker-compose -f src/main/docker/mongodb.yml down
CALL docker-compose -f src/main/docker/keycloak.yml down
CALL docker ps
CALL docker-compose -f src/main/docker/mongodb.yml up -d
CALL docker-compose -f src/main/docker/keycloak.yml up -d
CALL docker ps
